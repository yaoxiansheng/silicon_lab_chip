<?xml version="1.0" encoding="ISO-8859-1" ?>

<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified">
  <xs:annotation>
    <xs:documentation xml:lang="en">
      Custom Symbol File schema for �C/Probe 
      (c) Copyright 2013; Micrium, Inc.; Weston, FL 
      Created by Antonio Namnum
    </xs:documentation>
  </xs:annotation>
  <!--
 ******************************** GLOBAL TYPES ******************************** 
-->
  <!--  Root element of xml that holds symbols.  -->
  <xs:element name="CustomSymbols" type="CustomSymbolsType">
    <xs:annotation>
      <xs:documentation>
        Root of xml file that will contain all custom symbols.
      </xs:documentation>
    </xs:annotation>
  </xs:element>
  <!--  Represents a custom symbol  -->
  <xs:element name="Symbol" type="Symbol_TYPE">
    <xs:annotation>
      <xs:documentation>Custom symbol.</xs:documentation>
    </xs:annotation>
  </xs:element>
  <!--
 Represents a module or group of symbols. This tag is useful to group symbols. 
-->
  <xs:element name="GroupOfSymbols" type="GroupOfSymbolsType">
    <xs:annotation>
      <xs:documentation>
        Group of symbols in case you want to group a specific set of symbols.
      </xs:documentation>
    </xs:annotation>
  </xs:element>
  <!--
 ****************************************************************************** 
-->
  <!--
 ******************************* Types ******************************* 
-->
  <xs:simpleType name="DataTypes_TYPE">
    <xs:restriction base="xs:string">
      <xs:enumeration value="char"/>
      <xs:enumeration value="signed char"/>
      <xs:enumeration value="unsigned char"/>
      <xs:enumeration value="short"/>
      <xs:enumeration value="short int"/>
      <xs:enumeration value="signed short"/>
      <xs:enumeration value="signed short int"/>
      <xs:enumeration value="unsigned short"/>
      <xs:enumeration value="unsigned short int"/>
      <xs:enumeration value="int"/>
      <xs:enumeration value="signed int"/>
      <xs:enumeration value="unsigned"/>
      <xs:enumeration value="unsigned int"/>
      <xs:enumeration value="long"/>
      <xs:enumeration value="long int"/>
      <xs:enumeration value="signed long"/>
      <xs:enumeration value="signed long int"/>
      <xs:enumeration value="unsigned long"/>
      <xs:enumeration value="unsigned long int"/>
      <xs:enumeration value="long long"/>
      <xs:enumeration value="long long int"/>
      <xs:enumeration value="signed long long"/>
      <xs:enumeration value="signed long long int"/>
      <xs:enumeration value="unsigned long long"/>
      <xs:enumeration value="unsigned long long int"/>
      <xs:enumeration value="float"/>
      <xs:enumeration value="double"/>
      <xs:enumeration value="long double"/>
      <xs:enumeration value="struct"/>
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="DataType_TYPE">
    <xs:simpleContent>
      <xs:extension base="DataTypes_TYPE">
        <xs:attribute name="Size" type="xs:unsignedInt" use="required">
          <xs:annotation>
            <xs:documentation>
              The size of the variable in bytes. If variable is a pointer then place the size of the pointer!
            </xs:documentation>
          </xs:annotation>
        </xs:attribute>
        <xs:attribute name="IsPointer" type="xs:boolean" use="optional">
          <xs:annotation>
            <xs:documentation>
              True if symbol data type is a pointer, false otherwise.
            </xs:documentation>
          </xs:annotation>
        </xs:attribute>
        <xs:attribute name="IsArray" type="xs:boolean" use="optional">
          <xs:annotation>
            <xs:documentation>
              True if symbol data type is an array, false otherwise.
            </xs:documentation>
          </xs:annotation>
        </xs:attribute>
        <xs:attribute name="ArrayLength" type="xs:int" use="optional">
          <xs:annotation>
            <xs:documentation>
              If symbol data type is an array then it is the number of items in the array.
            </xs:documentation>
          </xs:annotation>
        </xs:attribute>
      </xs:extension>
    </xs:simpleContent>
  </xs:complexType>
  <xs:simpleType name="MemoryAddress_TYPE">
    <xs:restriction base="xs:string">
      <xs:pattern value="(?(?=^0x)0x[0-9a-fA-F]+|\d{1,10})"/>
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="Symbol_TYPE">
    <xs:sequence>
      <xs:element name="Name" type="xs:string">
        <xs:annotation>
          <xs:documentation>Name of the variable as declared in your C code.</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="DisplayName" type="xs:string" minOccurs="0">
        <xs:annotation>
          <xs:documentation>Name of the variable for display purposes.</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="DataType" type="DataType_TYPE">
        <xs:annotation>
          <xs:documentation>ANSI C data type as declared in your C code.</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="MemoryAddress" type="MemoryAddress_TYPE">
        <xs:annotation>
          <xs:documentation>
            Memory address in either decimal or hexadecimal format (0x1234).
          </xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="DataMembers" type="MultipleSymbolsType" minOccurs="0">
        <xs:annotation>
          <xs:documentation>
            Struct elements root tag in case the data type is a struct.
          </xs:documentation>
        </xs:annotation>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="MultipleSymbolsType">
    <xs:sequence>
      <xs:element ref="Symbol" minOccurs="1" maxOccurs="unbounded"/>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="GroupOfSymbolsType">
    <xs:sequence>
      <xs:element name="GroupName" type="xs:string">
        <xs:annotation>
          <xs:documentation>The name of the group.</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="GroupDisplayName" type="xs:string" minOccurs="0">
        <xs:annotation>
          <xs:documentation>Friendly name of the group</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element ref="GroupOfSymbols" minOccurs="0">
        <xs:annotation>
          <xs:documentation>If you wish to have another level of grouping.</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="Symbols" type="MultipleSymbolsType" minOccurs="0">
        <xs:annotation>
          <xs:documentation>Group of custom symbols.</xs:documentation>
        </xs:annotation>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="CustomSymbolsType">
    <xs:choice minOccurs="0" maxOccurs="unbounded">
      <xs:element ref="Symbol"/>
      <xs:element ref="GroupOfSymbols" />
    </xs:choice>   
  </xs:complexType>
  <!--
 ************************************************************************************************************************* 
-->
</xs:schema>