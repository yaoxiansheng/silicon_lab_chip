var NAVTREE =
[
  [ "RAIL", "index.html", [
    [ "RAIL Library", "index.html", null ],
    [ "Changelist", "md_docs_changelist.html", null ],
    [ "EFR32", "efr32_main.html", null ],
    [ "RAIL Multiprotocol", "md_docs_multiprotocol.html", [
      [ "Overview", "md_docs_multiprotocol.html#rail_multiprotocol", null ],
      [ "Radio Scheduler", "md_docs_multiprotocol.html#rail_multiprotocol_radio_scheduler", [
        [ "Yielding the radio", "md_docs_multiprotocol.html#rail_radio_scheduler_yield", null ]
      ] ],
      [ "Building a Multiprotocol Application", "md_docs_multiprotocol.html#rail_multiprotocol_building", null ]
    ] ],
    [ "Modules", "modules.html", "modules" ],
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Structure Index", "classes.html", null ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ],
      [ "Globals", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", null ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", null ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ],
    [ "silabs.com", "^http://www.silabs.com", null ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html",
"group___diagnostic.html#gad985f53aa4aa7a1bf82630eb18eaa681",
"group___i_e_e_e802__15__4.html#gacfd7df494c54818db8a6660171451af9",
"group___receive.html#gga83961c32832eb32624024fdbe9a114dbac9d724e962e6591b7939d7efb6f6f8c3",
"struct_r_a_i_l___channel_config__t.html#aad0f9ae91e61b7d030e3cf06734953f9"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';