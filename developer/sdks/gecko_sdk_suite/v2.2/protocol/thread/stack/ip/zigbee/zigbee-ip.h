// File: zigbee-ip.h
//
// Description: Constants and whatnot from the ZigBee IP specification.
//
// Author(s): Matteo Paris <matteo@ember.com>
//
// Copyright 2010 by Ember Corporation.  All rights reserved.               *80*

#ifndef ZIGBEE_IP_H
#define ZIGBEE_IP_H

#define ZIGBEE_MAX_NETWORK_ADDRESS      0xFFFD

#endif // ZIGBEE_IP_H
