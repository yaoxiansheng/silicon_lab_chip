/*
 * File: network-diagnostics.c
 * Description: Thread Diagnostic Functionality
 *
 * Copyright 2016 by Silicon Laboratories. All rights reserved.             *80*
 */

#include "core/ember-stack.h"
#include "hal/hal.h"
#include "framework/ip-packet-header.h"
#include "mac/mac-header.h"
#include "app/coap/coap.h"
#include "zigbee/child-data.h"
#include "app/util/counters/counters.h"
#include "stack/ip/network-diagnostics.h"
#include "stack/ip/ip-address.h"
#include "stack/ip/tls/debug.h"
#include "stack/ip/commission.h"

// for size_t
#include <stddef.h>
// for strncmp() and strlen()
#include <string.h>

typedef const struct {
  uint16_t offset;
  uint8_t tlv;
  uint8_t type;
  uint8_t length;
} GenericTlvDataDescription;

// A macro to get the offset of a field within an network diagnostic response
#define DIAG_DATA_OFFSET(f) \
  ((uint16_t) (long) &(((EmberDiagnosticData *) NULL)->f))

#define DIAG_DATA_FIELD(tlv, f, type, length) \
  { DIAG_DATA_OFFSET(f), tlv, (type), (length) }

#define DUMMY_FIELD() { 0, 0, 0, 0 }

// the order of these fields must match the TLV enumeration !
static const GenericTlvDataDescription diagnosticDataDescription[] = {
  DIAG_DATA_FIELD(DIAGNOSTIC_MAC_EXTENDED_ADDRESS, macExtendedAddress, POINTER, 8),
  DIAG_DATA_FIELD(DIAGNOSTIC_ADDRESS_16, address16, INT16, 0),
  DIAG_DATA_FIELD(DIAGNOSTIC_MODE, mode, INT8, 0),
  DIAG_DATA_FIELD(DIAGNOSTIC_TIMEOUT, timeout, INT16, 0),
  DIAG_DATA_FIELD(DIAGNOSTIC_CONNECTIVITY, connectivity, TLV, 0),
  DIAG_DATA_FIELD(DIAGNOSTIC_ROUTING_TABLE, routingTable, TLV, 0),
  DIAG_DATA_FIELD(DIAGNOSTIC_LEADER_DATA, leaderData, TLV, 0),
  DIAG_DATA_FIELD(DIAGNOSTIC_NETWORK_DATA, networkData, TLV, 0),
  DIAG_DATA_FIELD(DIAGNOSTIC_IPV6_ADDRESS_LIST, ipv6AddressList, TLV, 0),
  DIAG_DATA_FIELD(DIAGNOSTIC_MAC_COUNTERS, macCounters, TLV, 0),
  DUMMY_FIELD(),
  DUMMY_FIELD(),
  DUMMY_FIELD(),
  DUMMY_FIELD(),
  DIAG_DATA_FIELD(DIAGNOSTIC_BATTERY_LEVEL, batteryLevel, INT8, 0),
  DIAG_DATA_FIELD(DIAGNOSTIC_VOLTAGE, voltage, INT16, 0),
  DIAG_DATA_FIELD(DIAGNOSTIC_CHILD_TABLE, childTable, TLV, 0),
  DIAG_DATA_FIELD(DIAGNOSTIC_CHANNEL_PAGES, channelPages, TLV, 0),
};

static bool parseTlvs(uint8_t *data,
                      const GenericTlvDataDescription tlvDataArray[],
                      uint8_t minTlvId,
                      uint8_t maxTlvId,
                      const uint8_t *payload,
                      uint16_t length)
{
  uint32_t tlvMask = 0;
  const uint8_t *finger;

  for (finger = payload;
       finger + 2 <= payload + length;
       finger += (finger[1] + 2)) {
    uint8_t tlvId = finger[0];
    uint16_t length = finger[1];
    const uint8_t *value = finger + 2;

    if (tlvId < minTlvId || tlvId > maxTlvId) {
      // we don't know what this is
      continue;
    }

    const GenericTlvDataDescription *tlvData = &tlvDataArray[tlvId - minTlvId];
    tlvMask |= BIT32(tlvId - minTlvId);
    void *field = ((uint8_t *)data) + tlvData->offset;

    switch (tlvData->type) {
      case INT8:
        if (length != 1) {
          lose(COAP, false);
        }
        *(uint8_t *)field = *value;
        break;

      case INT16:
        if (length != 2) {
          lose(COAP, false);
        }
        *((uint16_t *) field) = emberFetchHighLowInt16u(value);
        break;

      case INT32:
        if (length != 4) {
          lose(COAP, false);
        }
        *((uint32_t *) field) = emberFetchHighLowInt32u(value);
        break;

      case POINTER:
        if (length != tlvData->length) {
          lose(COAP, false);
        }
        *((const uint8_t **) field) = value;
        break;

      case TLV:
        *((const uint8_t **) field) = value - 1;
        break;
    }
  }

  *((uint32_t *) data) = tlvMask;
  return true;
}

bool emberParseDiagnosticData(EmberDiagnosticData *data,
                              const uint8_t *payload,
                              uint16_t length)
{
  MEMSET(data, 0, sizeof(EmberDiagnosticData));
  return parseTlvs((uint8_t *)data,
                   diagnosticDataDescription,
                   DIAGNOSTIC_MAC_EXTENDED_ADDRESS,
                   DIAGNOSTIC_CHANNEL_PAGES,
                   payload,
                   length);
}

// A macro to get the offset of a field within an mgmt get response
#define COMM_DATA_OFFSET(f) \
  ((uint16_t) (long) &(((EmberCommissionData *) NULL)->f))

#define COMM_DATA_FIELD(tlv, f, type, length) \
  { COMM_DATA_OFFSET(f), tlv, (type), (length) }

// the order of these fields must match the TLV enumeration !
static const GenericTlvDataDescription commissionDataDescription[] = {
  COMM_DATA_FIELD(COMMISSION_PROVISIONING_URL_TLV, provisioningUrl, TLV, 0),
  COMM_DATA_FIELD(COMMISSION_VENDOR_NAME_TLV, vendorName, TLV, 0),
  COMM_DATA_FIELD(COMMISSION_VENDOR_MODEL_TLV, vendorModel, TLV, 0),
  COMM_DATA_FIELD(COMMISSION_VENDOR_SW_VERSION_TLV, swVersion, TLV, 0),
  COMM_DATA_FIELD(COMMISSION_VENDOR_DATA_TLV, vendorData, TLV, 0),
  COMM_DATA_FIELD(COMMISSION_VENDOR_STACK_VERSION_TLV, vendorStackVersion, TLV, 0),
};

bool emberParseManagementGetData(EmberCommissionData *data,
                                 const uint8_t *payload,
                                 uint16_t length)
{
  MEMSET(data, 0, sizeof(EmberCommissionData));
  return parseTlvs((uint8_t *)data,
                   commissionDataDescription,
                   COMMISSION_PROVISIONING_URL_TLV,
                   COMMISSION_VENDOR_STACK_VERSION_TLV,
                   payload,
                   length);
}

static uint8_t *addTypeListTlv(uint8_t *finger,
                               const uint8_t *tlvs,
                               uint8_t tlvsLength)
{
  *finger++ = DIAGNOSTIC_TYPE_LIST;
  *finger++ = tlvsLength;
  MEMCOPY(finger, tlvs, tlvsLength);
  finger += tlvsLength;
  return finger;
}

static void responseHandler(EmberCoapStatus status,
                            EmberCoapCode code,
                            EmberCoapReadOptions *options,
                            uint8_t *payload,
                            uint16_t payloadLength,
                            EmberCoapResponseInfo *info)
{
  if (status == EMBER_COAP_MESSAGE_RESPONSE) {
    emberDiagnosticAnswerHandler(EMBER_SUCCESS,
                                 &info->remoteAddress,
                                 payload,
                                 payloadLength);
  }
}

#define MAX_DIAG_REQUEST_LENGTH 14 // Currently supporting 14 TLVs

void emberSendDiagnostic(const EmberIpv6Address *destination,
                         const uint8_t *requestedTlvs,
                         uint8_t length,
                         const uint8_t *uri)
{
  uint8_t payload[MAX_DIAG_REQUEST_LENGTH + 2];
  uint8_t *finger = payload;

  if (length == 0
      || length > MAX_DIAG_REQUEST_LENGTH) {
    emberDiagnosticAnswerHandler(EMBER_ERR_FATAL, NULL, NULL, 0);
  } else {
    finger = addTypeListTlv(finger, requestedTlvs, length);
  }

  assert(finger - payload <= sizeof(payload));
  CoapMessage message;
  MEMSET(&message, 0, sizeof(CoapMessage));
  message.type = (emIsMulticastAddress(destination->bytes)
                  ? COAP_TYPE_NON_CONFIRMABLE
                  : COAP_TYPE_CONFIRMABLE);
  MEMCOPY(message.remoteAddress.bytes, destination->bytes, 16);
  message.code = EMBER_COAP_CODE_POST;
  message.localPort = THREAD_COAP_PORT;
  message.remotePort = THREAD_COAP_PORT;
  message.payload = payload;
  message.payloadLength = finger - payload;
  if (emStrcmp(uri, (const uint8_t *)DIAGNOSTIC_GET_URI) == 0) {
    message.responseHandler = responseHandler;
  }
  emStoreDefaultIpAddress(message.localAddress.bytes);
  if (emSubmitCoapMessage(&message, (const uint8_t *)uri, NULL_BUFFER)
      != EMBER_SUCCESS) {
    emberDiagnosticAnswerHandler(EMBER_ERR_FATAL, NULL, NULL, 0);
  }
}

void emberSendDiagnosticGet(const EmberIpv6Address *destination,
                            const uint8_t *requestedTlvs,
                            uint8_t length)
{
  emberSendDiagnostic(destination,
                      requestedTlvs,
                      length,
                      (const uint8_t *)DIAGNOSTIC_GET_URI);
}

void emberSendDiagnosticQuery(const EmberIpv6Address *destination,
                              const uint8_t *requestedTlvs,
                              uint8_t length)
{
  emberSendDiagnostic(destination,
                      requestedTlvs,
                      length,
                      (const uint8_t *)DIAGNOSTIC_QUERY_URI);
}

void emberSendDiagnosticReset(const EmberIpv6Address *destination,
                              const uint8_t *requestedTlvs,
                              uint8_t length)
{
  emberSendDiagnostic(destination,
                      requestedTlvs,
                      length,
                      (const uint8_t *)DIAGNOSTIC_RESET_URI);
}

#define MAX_MGMT_GET_REQUEST_LENGTH 6  // Currently supporting 6 TLVs

EmberStatus emberSendManagementGetRequest(const EmberIpv6Address *destination,
                                          const uint8_t *requestedTlvs,
                                          uint8_t length)
{
  if (length > MAX_MGMT_GET_REQUEST_LENGTH) {
    return EMBER_BAD_ARGUMENT;
  }
  EmberCoapSendInfo info;
  MEMSET(&info, 0, sizeof(EmberCoapSendInfo));
  info.remotePort = THREAD_COAP_PORT;
  uint8_t getTlv[MAX_MGMT_GET_REQUEST_LENGTH + 2];
  getTlv[0] = COMMISSION_GET_TLV;
  getTlv[1] = length;
  MEMCOPY(getTlv + 2, requestedTlvs, length);

  return emberCoapPost(destination,
                       (const uint8_t *)MANAGEMENT_GET_URI,
                       getTlv,
                       length + 2,
                       emberManagementGetResponseHandler,
                       &info);
}
