var modules =
[
    [ "Thread Stack API Reference", "group__ember.html", "group__ember" ],
    [ "Hardware Abstraction Layer (HAL) API Reference", "group__hal.html", "group__hal" ],
    [ "Application Framework API Reference", "group__appframework.html", "group__appframework" ]
];