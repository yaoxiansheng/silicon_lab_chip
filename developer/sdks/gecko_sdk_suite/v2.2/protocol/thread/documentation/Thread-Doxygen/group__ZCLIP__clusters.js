var group__ZCLIP__clusters =
[
    [ "EmberZclClusterSpec_t", "structEmberZclClusterSpec__t.html", [
      [ "id", "structEmberZclClusterSpec__t.html#ae88d4078118e042ee55496bf0dde4fde", null ],
      [ "manufacturerCode", "structEmberZclClusterSpec__t.html#a21c55a4f638bc3316562a3fd9ff391da", null ],
      [ "role", "structEmberZclClusterSpec__t.html#a399617b65c7ad03fc8b8bbfb192e6502", null ]
    ] ],
    [ "EMBER_ZCL_CLUSTER_NULL", "group__ZCLIP__clusters.html#ga0e8755ae5c28acd447f48bd879a83575", null ],
    [ "EMBER_ZCL_MANUFACTURER_CODE_NULL", "group__ZCLIP__clusters.html#ga7e480ceec8b9203a166975195c8a6823", null ],
    [ "EmberZclClusterId_t", "group__ZCLIP__clusters.html#gac1fbb5eea21a535758ceb20876d1f48e", null ],
    [ "EmberZclManufacturerCode_t", "group__ZCLIP__clusters.html#gaa25ec0cb8b153815e9285c1c2a2ac940", null ],
    [ "EmberZclRole_t", "group__ZCLIP__clusters.html#ga53d04af31359eab74423723dffde3d59", [
      [ "EMBER_ZCL_ROLE_CLIENT", "group__ZCLIP__clusters.html#gga53d04af31359eab74423723dffde3d59a6b0fa32384ce3003233340e80e8a25ed", null ],
      [ "EMBER_ZCL_ROLE_SERVER", "group__ZCLIP__clusters.html#gga53d04af31359eab74423723dffde3d59a2ce69de647026b20b3b0812948b9ebe7", null ]
    ] ],
    [ "emberZclAreClusterSpecsEqual", "group__ZCLIP__clusters.html#ga2c58ed8e7e413e2a996548a8d8f82576", null ],
    [ "emberZclCompareClusterSpec", "group__ZCLIP__clusters.html#ga2824914342f187d50aa474e552e021b1", null ],
    [ "emberZclReverseClusterSpec", "group__ZCLIP__clusters.html#gaccde50b2126d655b1314f8c0a183ec24", null ]
];