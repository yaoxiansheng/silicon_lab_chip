var group__ZCLIP__addresses =
[
    [ "EmberZclUid_t", "structEmberZclUid__t.html", [
      [ "bytes", "structEmberZclUid__t.html#a86a7cdb2adffa3a70500991a6da1f90d", null ]
    ] ],
    [ "EmberZclCoapEndpoint_t", "structEmberZclCoapEndpoint__t.html", [
      [ "address", "structEmberZclCoapEndpoint__t.html#a72c5eb2676a88067b44d3db91236a8b3", null ],
      [ "flags", "structEmberZclCoapEndpoint__t.html#a0e0b90eb94dfc3560a8eabd7403e8c2d", null ],
      [ "port", "structEmberZclCoapEndpoint__t.html#a1523c896f5ed7dee5a04ef78efc4926c", null ],
      [ "uid", "structEmberZclCoapEndpoint__t.html#aac5e824fb1d417e272a5cb66c27240a8", null ]
    ] ],
    [ "EmberZclApplicationDestination_t", "structEmberZclApplicationDestination__t.html", [
      [ "data", "structEmberZclApplicationDestination__t.html#a8006efedaeb5a0fe9c6b3deaa2af765d", null ],
      [ "endpointId", "structEmberZclApplicationDestination__t.html#a249323180c92485338b767dee1485700", null ],
      [ "groupId", "structEmberZclApplicationDestination__t.html#a621839aae0a6d9e71ca0e2e1c86c1d38", null ],
      [ "type", "structEmberZclApplicationDestination__t.html#a85b382089dfd7f1ad0a08d5b547a2044", null ]
    ] ],
    [ "EmberZclDestination_t", "structEmberZclDestination__t.html", [
      [ "application", "structEmberZclDestination__t.html#af1abbdbd1ccbc5fd0d7ac7ea8ec58c3b", null ],
      [ "network", "structEmberZclDestination__t.html#a1885d8380e5818d621f837032e0b59c3", null ]
    ] ],
    [ "EMBER_ZCL_UID_BASE64URL_LENGTH", "group__ZCLIP__addresses.html#gac2524547b418fa80ef5a337f973adedb", null ],
    [ "EMBER_ZCL_UID_BASE64URL_SIZE", "group__ZCLIP__addresses.html#ga8738cb1f2a544123ca0649d968acc972", null ],
    [ "EMBER_ZCL_UID_BITS", "group__ZCLIP__addresses.html#ga008578b19e31f4e887699a7f1cf105b4", null ],
    [ "EMBER_ZCL_UID_SIZE", "group__ZCLIP__addresses.html#ga6fb4692cb4ea505a191d1e41d4badb51", null ],
    [ "EMBER_ZCL_UID_STRING_LENGTH", "group__ZCLIP__addresses.html#ga440cae0093f28a44e6d6ca143893b46d", null ],
    [ "EMBER_ZCL_UID_STRING_SIZE", "group__ZCLIP__addresses.html#ga4723a880cb784f2bdb9a25c9a48038c4", [
      [ "EMBER_ZCL_NO_FLAGS", "group__ZCLIP__addresses.html#gga06fc87d81c62e9abb8790b6e5713c55baf8e83e43eb4da0a920a95b6346785fc1", null ],
      [ "EMBER_ZCL_USE_COAPS_FLAG", "group__ZCLIP__addresses.html#gga06fc87d81c62e9abb8790b6e5713c55ba841d2803849358c6b6887642e57b8289", null ],
      [ "EMBER_ZCL_HAVE_IPV6_ADDRESS_FLAG", "group__ZCLIP__addresses.html#gga06fc87d81c62e9abb8790b6e5713c55bada585dfad99abbe089c964af6d7c1086", null ],
      [ "EMBER_ZCL_HAVE_UID_FLAG", "group__ZCLIP__addresses.html#gga06fc87d81c62e9abb8790b6e5713c55ba1f8f04ddf37d48322405fa4add2dba8e", null ]
    ] ],
    [ "EmberZclApplicationDestinationType_t", "group__ZCLIP__addresses.html#ga5a8c4b3c0bc989ada9c86b48ba7feaa3", [
      [ "EMBER_ZCL_APPLICATION_DESTINATION_TYPE_ENDPOINT", "group__ZCLIP__addresses.html#gga5a8c4b3c0bc989ada9c86b48ba7feaa3ab458831d5fd2869c8e24ee4257e1975a", null ],
      [ "EMBER_ZCL_APPLICATION_DESTINATION_TYPE_GROUP", "group__ZCLIP__addresses.html#gga5a8c4b3c0bc989ada9c86b48ba7feaa3acedd35dadd679e774e0bd2e74b94d614", null ]
    ] ]
];