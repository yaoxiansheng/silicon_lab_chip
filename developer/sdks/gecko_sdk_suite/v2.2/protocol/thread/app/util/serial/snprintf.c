#include PLATFORM_HEADER
#include "core/ember-stack.h"
#include "snprintf.h"

// MISRA does not allow the use of stdio.h or variadic functions.
// This is a version of snprintf() that uses a variadic macro, which
// MISRA does allow, in place of a variadic function.  See snprintf.h
// for more information.

static void addChars(PrintBuffer *buffer, const char *string, size_t length)
{
  if (buffer->finger < buffer->limit) {
    size_t remaining = buffer->limit - buffer->finger;
    size_t copyLength = length < remaining ? length : remaining;
    MEMMOVE(buffer->finger, string, copyLength);
  }
  buffer->finger += length;
}

// This is enough for a 64-bit integer with a sign
#define MAX_INTEGER_WIDTH 21

// Printing an integer.  This is the only complicated bit.
//  'conversion' is 'd', 'u', 'x', or 'X' from the format string.
//  'signChar' is '+', '-', or 0 (meaning no sign character).
//  'minWidth' is the minium width and 'padChar' is the character to use
//    for padding the result out to the minimum width.
static void printInt(PrintBuffer *buffer,
                     uint8_t conversion,
                     char signChar,
                     uint8_t minWidth,
                     char padChar,
                     uint32_t n)
{
  const char *chars = (conversion == 'X'
                       ? "0123456789ABCDEF"
                       : "0123456789abcdef");
  uint8_t base = ((conversion == 'x'
                   || conversion == 'X')
                  ? 16
                  : 10);
  char temp[MAX_INTEGER_WIDTH];
  // We write the number from right to left.
  char *finger = temp + MAX_INTEGER_WIDTH;
  if (signChar == '-') {        // calculate using negative numbers
    int32_t s = (int32_t) n;
    do {
      *--finger = chars[-(s % base)];
      s /= base;
    } while (s != 0);
  } else {                      // calculate using positive numbers
    do {
      *--finger = chars[n % base];
      n /= base;
    } while (n != 0);
  }

  if (signChar != 0) {
    *--finger = signChar;
  }

  uint8_t width = (temp + MAX_INTEGER_WIDTH) - finger;
  if (width < minWidth) {
    uint16_t add = minWidth - width;
    if (padChar == '0' && signChar != 0) {
      // If padChar is '0' the sign goes before the padding
      finger += 1;      // remove the sign that we wrote earlier
      width -= 1;
      addChars(buffer, &signChar, 1);  // rewrite sign as next char
    }
    if (buffer->finger < buffer->limit) {
      size_t remaining = buffer->limit - buffer->finger;
      size_t setLength = add < remaining ? add : remaining;
      MEMSET(buffer->finger, padChar, setLength);
    }
    buffer->finger += add;
  }

  addChars(buffer, finger, width);
}

bool emPbPrintfFunction(PrintBuffer *buffer,
                        const char *format,
                        const PrintfArg *args)
{
  if (buffer->error) {
    return false;       // an error occurred earlier, don't continue
  }
  buffer->error = true;         // save code by doing this up front

  bool copying = true;          // not processing a % directive
  // These are initialized when 'copying' is set to false.
  char padChar;
  uint32_t minWidth;
  bool haveWidth;

  for (; *format != 0; format++) {
    if (copying) {
      if (*format != '%') {
        addChars(buffer, format, 1);
      } else if (format[1] == '%') {
        addChars(buffer, format, 1);
        format += 1;
      } else {
        copying = false;
        padChar = ' ';
        minWidth = 0;
        haveWidth = false;
      }
    } else if ('1' <= *format
               && *format <= '9') {
      minWidth = minWidth * 10 + (*format - '0');
      haveWidth = true;
    } else if (*format == '0') {
      if (haveWidth) {
        minWidth *= 10;
      } else {
        padChar = '0';
      }
    } else {
      switch (*format) {
        case 's':
          if (args->type != PRINTF_STRING) {
            goto lose;
          }
          const char *s = args->value.string;
          addChars(buffer, s, strlen(s));
          break;

        case 'c':
          if (args->type != PRINTF_CHAR) {
            goto lose;
          }
          char c = args->value.ch;
          addChars(buffer, &c, 1);
          break;

        case 'd':
          if (args->type != PRINTF_INT) {
            goto lose;
          }
          int n = args->value.integer;
          char sign = (n < 0
                       ? '-'
                       : 0);
          printInt(buffer, 'd', sign, minWidth, padChar, n);
          break;

        case 'u':
        case 'x':
        case 'X': {
          if (args->type != PRINTF_UINT) {
            goto lose;
          }
          int n = args->value.unsignedInt;
          printInt(buffer, *format, 0, minWidth, padChar, n);
          break;
        }
        default:
          goto lose;
      }
      args += 1;
      copying = true;
    }
  }

  // Check that we didn't end in the middle of a % and that there are
  // no leftover arguments.
  if (!(copying && args->type == PRINTF_END)) {
    goto lose;
  }

  buffer->error = false;        // no errors occured

  lose:
  // Put a nul at the end of whatever we were able to print.
  if (buffer->finger < buffer->limit) {
    *buffer->finger = 0;
  } else {
    *(buffer->limit - 1) = 0;
  }

  return !buffer->error;
}

// A wrapper that provides the same interface as the standard snprintf().
int emSnprintfFunction(char *to,
                       size_t length,
                       const char *format,
                       const PrintfArg *args)
{
  PrintBuffer b = { .finger = to,
                    .limit = to + length,
                    .error = false };
  if (emPbPrintfFunction(&b, format, args)) {
    return b.finger - to;
  } else {
    return -1;
  }
}
