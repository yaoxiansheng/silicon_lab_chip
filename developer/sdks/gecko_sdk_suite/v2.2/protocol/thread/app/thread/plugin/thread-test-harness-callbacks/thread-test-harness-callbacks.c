// Copyright 2017 Silicon Laboratories, Inc.

// This plugin provides all of the CLI definitions needed for
// to test against the GRL Thread Test Harness.

#include PLATFORM_HEADER
#include CONFIGURATION_HEADER
#include EMBER_AF_API_STACK
#include EMBER_AF_API_DEBUG_PRINT
#include EMBER_AF_API_HAL
#include EMBER_AF_API_COMMAND_INTERPRETER2

#include "app/coap/coap.h"
#include "stack/core/ember-stack.h"
#include "stack/ip/zigbee/join.h"
#include "stack/ip/zigbee/child-data.h"
#include "stack/ip/zigbee/key-management.h"
#include "stack/routing/neighbor/neighbor.h"
#include "stack/ip/ip-address.h"
#include "stack/ip/network-data.h"
#include "stack/ip/mle.h"
#include "stack/ip/tls/dtls-join.h"
#include "stack/ip/commission.h"
#include "stack/mac/802.15.4/802-15-4-ccm.h"
#include "stack/ip/address-management.h"
#include "stack/ip/rip.h"
#include "stack/ip/network-fragmentation.h"
#include "stack/ip/local-server-data.h"
#include "stack/ip/commission-dataset.h"
#include "stack/ip/association.h"
#include "stack/core/ember-stack.h"
#include "stack/framework/ip-packet-header.h"
#include "stack/ip/dispatch.h"
#include "app/util/serial/command-interpreter2-util.h"
#include "app/util/ip/print-utilities.h"

// Macros
// ============================================================================
#ifndef TTH
  #define TTH(x) x
#endif

#define EMBER_NETWORK_DATA_HAS_ROUTE     0
#define EMBER_NETWORK_DATA_PREFIX        1

// Defined in ip-address.h, which can't be included on the host.
#ifndef MIN_VALID_LIFETIME_SEC
  #define MIN_VALID_LIFETIME_SEC 60
#endif

#define emberNetworkDataType(tlv) ((tlv)[0] >> 1)

// Globals
// ============================================================================
extern EmberNodeType emNodeType;
extern uint8_t emForcedSlaacAddress[];
extern bool emForceSlaacAddress;

static uint8_t networkDataBuffer[255];
static const uint8_t coapTestUri[] = "coap-test";

// Forward declarations
// ============================================================================
static void printOk(void);
static void displayNetworkData(const uint8_t *networkData, uint16_t length);

// Utility Functions
// ============================================================================
// print functions
static void printOk(void)
{
  emberAfAppPrintln("OK.");
}

static void displayNetworkData(const uint8_t *networkData, uint16_t length)
{
  const uint8_t *finger = networkData;
  const uint8_t *end = finger + length;
  const uint8_t *outerTlv = NULL;
  bool hasRoute = false;

  while (finger < end) {
    uint16_t length = finger[1];
    const uint8_t *next = finger + 2 + length;
    switch (emberNetworkDataType(finger)) {
      case EMBER_NETWORK_DATA_PREFIX:
        outerTlv = finger;
        next = (uint8_t *)(emberPrefixBits(finger) + emberPrefixLengthInBytes(finger));
        hasRoute = false;
        break;
      case EMBER_NETWORK_DATA_HAS_ROUTE:
        if (!hasRoute && outerTlv != NULL) {
          emberAfAppPrint("external route: prefix: ");
          printHexWords(emberPrefixBits(outerTlv),
                        emberPrefixLengthInBytes(outerTlv) >> 1);
          emberAfAppPrintln("/%d | available", emberPrefixLengthInBits(outerTlv));
          hasRoute = true;
        }
    }
    finger = next;
  }
}

// Callbacks/Return functions and Handlers
// ============================================================================
void TTH(emberNetworkStatusHandler)(EmberNetworkStatus newNetworkStatus, EmberNetworkStatus oldNetworkStatus, EmberJoinFailureReason reason);

void TTH(emberPollForDataReturn)(EmberStatus status)
{
  if (status != EMBER_SUCCESS) {
    emberAfAppPrintln("poll failed, status:0x%x", status);
  }
}

// to print the ICMP Ping reply
void TTH(emberIncomingIcmpHandler)(Ipv6Header * ipHeader)
{
  if (ipHeader->icmpType == ICMP_DESTINATION_UNREACHABLE) {
    emberAfAppPrintln("ICMP RX: DEST UNREACHABLE");
  } else {
    emberAfAppPrint("ICMP RX: ");
    printIpPacket(ipHeader);
  }
}

void TTH(emberGetGlobalPrefixReturn)(uint8_t flags,
                                     bool isStable,
                                     const uint8_t * prefix,
                                     uint8_t prefixLengthInBits,
                                     uint8_t domainId,
                                     uint32_t preferredLifetime,
                                     uint32_t validLifetime)
{
  if (prefixLengthInBits == 0) {
    emberAfAppPrintln("no more global prefixes.");
  } else {
    uint8_t prefixLengthInBytes = ((prefixLengthInBits + 7) >> 3);
    emberAfAppPrint("temp: %p | prefix: ", isStable ? "no" : "yes");
    printHexWords(prefix, prefixLengthInBytes >> 1);
    emberAfAppPrint(" | domain id: %d | p-time: %d | v-time: %d",
                    domainId,
                    preferredLifetime,
                    validLifetime);
    emberAfAppPrintln("");
  }
}

void TTH(emberGetGlobalAddressReturn)(const EmberIpv6Address * address,
                                      uint32_t preferredLifetime,
                                      uint32_t validLifetime,
                                      LocalServerFlag addressFlags)
{
  if (addressFlags == 0) {
    emberAfAppPrintln("no more global addresses.");
  } else {
    emberAfAppPrint("%p gua: ",
                    (addressFlags & EMBER_GLOBAL_ADDRESS_DHCP
                     || addressFlags & EMBER_GLOBAL_ADDRESS_AM_DHCP_SERVER
                     ? "dhcp"
                     : (addressFlags & EMBER_GLOBAL_ADDRESS_SLAAC
                        || addressFlags & EMBER_GLOBAL_ADDRESS_AM_SLAAC_SERVER
                        ? "slaac"
                        : "unknown")));
    printIpAddress(address->bytes);
    emberAfAppPrintln("/64");
  }
}

void TTH(emberFormNetworkReturn)(EmberStatus status)
{
  emberAfAppPrintln("form return status:0x%x", status);
}

void TTH(emberJoinNetworkReturn)(EmberStatus status)
{
  emberAfAppPrintln("join return status:0x%x", status);
}

void TTH(emberAttachToNetworkReturn)(EmberStatus status)
{
  emberAfAppPrintln("attach return status:0x%x", status);
}

void TTH(emberResumeNetworkReturn)(EmberStatus status)
{
  emberAfAppPrintln("resume return status:0x%x", status);
}

void TTH(emberGetNetworkDataReturn)(EmberStatus status,
                                    uint8_t * networkData,
                                    uint16_t length)
{
  if (status == EMBER_SUCCESS) {
    displayNetworkData(networkData, length);
  } else {
    emberAfAppPrintln("emberGetNetworkData failed - status 0x%x", status);
  }
}

void TTH(emberGetNetworkDataTlvReturn)(uint8_t type,
                                       uint8_t index,
                                       uint8_t versionNumber,
                                       const uint8_t * tlv,
                                       uint8_t tlvLength)
{
  emberAfAppPrintln("type: 0x%x | "
                    "index: %d | "
                    "versionNumber: 0x%x | "
                    "tlvLength: %d",
                    type,
                    index,
                    versionNumber,
                    tlvLength);
  printTlvs(tlv, tlv + tlvLength);
}

void TTH(emberConfigureGatewayReturn)(EmberStatus status)
{
  if (status == EMBER_SUCCESS) {
    printOk();
  } else {
    emberAfAppPrintln("Failed. Status: 0x%x", status);
  }
}

void TTH(emberSetCommissionerKeyReturn)(EmberStatus status)
{
  if (status == EMBER_SUCCESS) {
    printOk();
  } else {
    emberAfAppPrintln("Invalid call. Status: %p",
                      networkStatusString(emberNetworkStatus()));
  }
}

void TTH(emberCommissionNetworkReturn)(EmberStatus status)
{
  // Do nothing. We just need this implemented so that the BRMA doesn't get called.
  if (status == EMBER_SUCCESS) {
    emberAfAppPrintln("NCP: Commission network successful");
  } else {
    emberAfAppPrintln("CommissionNetwork failed 0x%x", status);
  }
}

void TTH(emberSetJoinKeyReturn)(EmberStatus status)
{
  if (status == EMBER_SUCCESS) {
    printOk();
  } else {
    emberAfAppPrintln("bad arguments.");
  }
}

void TTH(emberDiagnosticAnswerHandler)(EmberStatus status,
                                       const EmberIpv6Address * remoteAddress,
                                       const uint8_t * payload,
                                       uint16_t payloadLength)
{
  if (status != EMBER_SUCCESS) {
    emberAfAppPrintln("Error: can't send diagnostic request, status: %u",
                      status);
    return;
  }

  EmberDiagnosticData diagnosticData;

  if (!emberParseDiagnosticData(&diagnosticData, payload, payloadLength)) {
    emberAfAppPrintln("Can't parse diagnostic data");
    return;
  }

  emberAfAppPrint("[Received diagnostic response");

  if (emberDiagnosticDataHasTlv(&diagnosticData, DIAGNOSTIC_MAC_EXTENDED_ADDRESS)) {
    emberAfAppPrint("\nMac extended address: ");
    emberAfAppPrintBuffer(diagnosticData.macExtendedAddress, 8, true);
    emberAfAppPrintln("");
  }

  if (emberDiagnosticDataHasTlv(&diagnosticData, DIAGNOSTIC_ADDRESS_16)) {
    emberAfAppPrintln("Address16: 0x%2X", diagnosticData.address16);
  }

  if (emberDiagnosticDataHasTlv(&diagnosticData, DIAGNOSTIC_MODE)) {
    emberAfAppPrintln("Mode: %u", diagnosticData.mode);
  }

  if (emberDiagnosticDataHasTlv(&diagnosticData, DIAGNOSTIC_TIMEOUT)) {
    emberAfAppPrintln("Timeout: %u", diagnosticData.timeout);
  }

  if (emberDiagnosticDataHasTlv(&diagnosticData, DIAGNOSTIC_CONNECTIVITY)) {
    emberAfAppPrint("connectivity: ");
    emberAfAppPrintBuffer(diagnosticData.connectivity + 1, diagnosticData.connectivity[0], true);
    emberAfAppPrintln("");
  }

  if (emberDiagnosticDataHasTlv(&diagnosticData, DIAGNOSTIC_ROUTING_TABLE)) {
    emberAfAppPrint("Routing table: ");
    emberAfAppPrintBuffer(diagnosticData.routingTable + 1, diagnosticData.routingTable[0], true);
    emberAfAppPrintln("");
  }

  if (emberDiagnosticDataHasTlv(&diagnosticData, DIAGNOSTIC_LEADER_DATA)) {
    emberAfAppPrint("Leader data: ");
    emberAfAppPrintBuffer(diagnosticData.leaderData + 1, diagnosticData.leaderData[0], true);
    emberAfAppPrintln("");
  }

  if (emberDiagnosticDataHasTlv(&diagnosticData, DIAGNOSTIC_NETWORK_DATA)) {
    emberAfAppPrint("Network data: ");
    emberAfAppPrintBuffer(diagnosticData.networkData + 1, diagnosticData.networkData[0], true);
    emberAfAppPrintln("");
  }

  if (emberDiagnosticDataHasTlv(&diagnosticData, DIAGNOSTIC_IPV6_ADDRESS_LIST)) {
    emberAfAppPrint("IPv6 address list: ");
    emberAfAppPrintBuffer(diagnosticData.ipv6AddressList + 1, diagnosticData.ipv6AddressList[0], true);
    emberAfAppPrintln("");
  }

  if (emberDiagnosticDataHasTlv(&diagnosticData, DIAGNOSTIC_MAC_COUNTERS)) {
    emberAfAppPrint("Mac counters list: ");
    emberAfAppPrintBuffer(diagnosticData.macCounters + 1, diagnosticData.macCounters[0], true);
    emberAfAppPrintln("");
  }

  if (emberDiagnosticDataHasTlv(&diagnosticData, DIAGNOSTIC_BATTERY_LEVEL)) {
    emberAfAppPrintln("Battery level: %u", diagnosticData.batteryLevel);
  }

  if (emberDiagnosticDataHasTlv(&diagnosticData, DIAGNOSTIC_VOLTAGE)) {
    emberAfAppPrintln("Voltage: %u", diagnosticData.voltage);
  }

  if (emberDiagnosticDataHasTlv(&diagnosticData, DIAGNOSTIC_CHILD_TABLE)) {
    emberAfAppPrint("Child table: ");
    emberAfAppPrintBuffer(diagnosticData.childTable + 1, diagnosticData.childTable[0], true);
    emberAfAppPrintln("");
  }

  if (emberDiagnosticDataHasTlv(&diagnosticData, DIAGNOSTIC_CHANNEL_PAGES)) {
    emberAfAppPrint("Channel pages: ");
    emberAfAppPrintBuffer(diagnosticData.channelPages + 1, diagnosticData.channelPages[0], true);
  }

  emberAfAppPrintln("]");
}

void TTH(emberAddressConfigurationChangeHandler)(const EmberIpv6Address * address,
                                                 uint32_t preferredLifetime,
                                                 uint32_t validLifetime,
                                                 uint8_t addressFlags)
{
  if (addressFlags != EMBER_LOCAL_ADDRESS) {
    emberAfAppPrint("new address%p: ",
                    (addressFlags & EMBER_GLOBAL_ADDRESS_DHCP
                     || addressFlags & EMBER_GLOBAL_ADDRESS_AM_DHCP_SERVER
                     ? " (dhcp)"
                     : (addressFlags & EMBER_GLOBAL_ADDRESS_SLAAC
                        || addressFlags & EMBER_GLOBAL_ADDRESS_AM_SLAAC_SERVER
                        ? " (slaac)"
                        : "")));
    printIpAddress(address->bytes);
    if (validLifetime <= MIN_VALID_LIFETIME_SEC) {
      emberAfAppPrintln(" is not valid anymore");
    } else {
      emberAfAppPrintln(" | valid lifetime: %d sec. | preferred lifetime: %d sec.",
                        validLifetime,
                        preferredLifetime);
    }
  }
}

void TTH(emberCoapRequestHandler)(EmberCoapCode code,
                                  uint8_t * uri,
                                  EmberCoapReadOptions * options,
                                  const uint8_t * payload,
                                  uint16_t payloadLength,
                                  const EmberCoapRequestInfo * info)
{
  if (strncmp((const char *) uri,
              (const char *) coapTestUri,
              sizeof(coapTestUri)) == 0) {
    emberAfAppPrint("[CoAP RX %s from ", emGetCoapCodeName(code));
    printIpAddress(info->remoteAddress.bytes);
    emberAfAppPrintln(" | uri: %s ]", uri);
  } else {
    emberAfAppPrint("{coap} [CoAP RX | %s | from ", emGetCoapCodeName(code));
    emberAfAppPrintBuffer(info->remoteAddress.bytes, 16, true);
    emberAfAppPrint(" | uri: %s | token", uri);
    emberAfAppPrintBuffer(info->token, info->tokenLength, true);
    emberAfAppPrint(" | payload ");
    emberAfAppPrintBuffer(payload, payloadLength, true);
    emberAfAppPrintln("]");
  }

  if (code == EMBER_COAP_CODE_POST) {
    emberCoapRespondWithCode(info, EMBER_COAP_CODE_204_CHANGED);
  }
}

void TTH(emberCommissionerStatusHandler)(uint16_t flags,
                                         const uint8_t * id,
                                         uint8_t idLength)
{
  if (flags & EMBER_HAVE_COMMISSIONER) {
    emberAfAppPrintln("%s commissioner, joining %sabled%s",
                      ((flags & EMBER_AM_COMMISSIONER) ? "am" : "have"),
                      ((flags & EMBER_JOINING_ENABLED) ? "en" : "dis"),
                      ((flags & EMBER_JOINING_WITH_EUI_STEERING)
                       ? " with steering" : ""));
  } else {
    emberAfAppPrintln("no commissioner");
  }
}

// Border router and DHCP server callbacks
void TTH(emberExternalRouteChangeHandler)(const uint8_t * prefix,
                                          uint8_t prefixLengthInBits,
                                          bool open)
{
  uint8_t prefixLengthInBytes = (prefixLengthInBits >> 3)
                                + ((prefixLengthInBits & 0x07) ? 1 : 0);

  emberAfAppPrint("external route: prefix: ");
  printHexWords(prefix, prefixLengthInBytes >> 1);
  emberAfAppPrintln("/%d | %p",
                    prefixLengthInBytes << 3,
                    open ? "available" : "unavailable");
}

void TTH(emberDhcpServerChangeHandler)(const uint8_t * prefix,
                                       uint8_t prefixLengthInBits,
                                       bool available)
{
  uint8_t prefixLengthInBytes = (prefixLengthInBits >> 3)
                                + ((prefixLengthInBits & 0x07) ? 1 : 0);

  emberAfAppPrint("dhcp server: prefix: ");
  printHexWords(prefix, prefixLengthInBytes >> 1);
  emberAfAppPrintln(" | %p", available ? "available" : "unavailable");
  if (available) {
    emberRequestDhcpAddress(prefix, prefixLengthInBits);
  }
}

void TTH(emberSlaacServerChangeHandler)(const uint8_t * prefix,
                                        uint8_t prefixLengthInBits,
                                        bool available)
{
  uint8_t prefixLengthInBytes = (prefixLengthInBits >> 3)
                                + ((prefixLengthInBits & 0x07) ? 1 : 0);

  emberAfAppPrint("slaac prefix: ");
  printHexWords(prefix, prefixLengthInBytes >> 1);
  emberAfAppPrintln(" | %p", available ? "available" : "unavailable");
  if (available) {
    if (emForceSlaacAddress) {
      emberAfAppPrint("Forcing SLAAC address to:");
      emberAfAppPrintBuffer(emForcedSlaacAddress, 16, true);
      emberAfAppPrintln("");
      emberRequestSlaacAddress(emForcedSlaacAddress, 128);
    } else {
      emberRequestSlaacAddress(prefix, prefixLengthInBits);
    }
  }
}

void TTH(emberNetworkDataChangeHandler)(const uint8_t * networkData, uint16_t length)
{
  // The callback may not include the actual data, in which case we have to send
  // a request for it.
  if (networkData == NULL) {
    emberGetNetworkData(networkDataBuffer, sizeof(networkDataBuffer));
  } else {
    displayNetworkData(networkData, length);
  }
}

// We save the URI of incoming requests so that we can print them out when
// we send the ACK.  It all happens in one go, so there is only one active
// URI at a time.
#define MAX_SAVED_URI 16
static uint8_t savedUri[MAX_SAVED_URI + 1];     // +1 for terminating null

void printThreadTestLogMessage(ThreadTestLogType type, ThreadTestLogData *data)
{
  switch (type) {
    case THREAD_TEST_LOG_STACK: {
      ThreadTestLogStackMessage *info = (ThreadTestLogStackMessage *) data;
      emberAfAppPrint(info->tx
                      ? "{coap} Stack CoAP TX | %s/%s | to:"
                      : "{coap} Stack CoAP RX | %s/%s | from:",
                      emGetCoapTypeName(info->type),
                      emGetCoapCodeName(info->code));
      emberAfAppPrintBuffer(info->requestInfo->remoteAddress.bytes, 16, true);
      emberAfAppPrint(" | uri: %s | port: %d | token:", info->uri, info->requestInfo->remotePort);
      emberAfAppPrintBuffer(info->requestInfo->token, info->requestInfo->tokenLength, true);
      emberAfAppPrint(" | length: %d | payload:", info->payloadLength);
      emberAfAppPrintBuffer(info->payload, info->payloadLength, true);
      emberAfAppPrintln("");
      break;
    }
    case THREAD_TEST_LOG_REQUEST_URI: {
      uint8_t *uri = (uint8_t *) data;
      uint16_t length = strlen(uri);
      if (MAX_SAVED_URI < length) {
        length = MAX_SAVED_URI;
      }
      MEMCOPY(savedUri, uri, length);
      savedUri[length] = 0;
      break;
    }
    case THREAD_TEST_LOG_COAP_TX: {
      ThreadTestLogTxMessage *info = (ThreadTestLogTxMessage *) data;
      emberAfAppPrint("{coap} TX message id: %u | uri: %s | type: %s | code: %s | port %d | to: ",
                      info->message->messageId,
                      (info->message->type == COAP_TYPE_ACK
                       ? savedUri
                       : info->uri),
                      emGetCoapTypeName(info->message->type),
                      emGetCoapCodeName(info->message->code),
                      info->message->remotePort);
      emberAfAppPrintBuffer(info->message->remoteAddress.bytes, 16, true);
      emberAfAppPrint(" | bytes: ");
      emberAfAppPrintBuffer(info->message->payload, info->message->payloadLength, true);
      emberAfAppPrint(" | extra: ");
      emberAfAppPrintBuffer(emGetBufferPointer(info->payloadExtraBuffer),
                            emGetBufferLength(info->payloadExtraBuffer),
                            true);
      emberAfAppPrintln("");
      break;
    }
    case THREAD_TEST_LOG_COAP_ACK: {
      ThreadTestLogAckUriMessage *info = (ThreadTestLogAckUriMessage *) data;
      emberAfAppPrint("{coap} CoAP RX message id: %u | uri: %s",
                      info->event->messageId,
                      emGetBufferPointer(info->event->uri));
      emberAfAppPrint(" | type: ACK | length: %d | payload:", info->message->payloadLength);
      emberAfAppPrintBuffer(info->message->payload, info->message->payloadLength, true);
      emberAfAppPrintln("");
      break;
    }
    default:
      break;
  }
}

#ifdef EMBER_AF_PLUGIN_NCP_LIBRARY
void TTH(emberCustomHostToNcpMessageHandler)(const uint8_t * message, uint16_t messageLength)
{
  // Make sure these variables stay false so we don't send icmp and DTLS messages to the host
  emSecurityToUart = false;
  emIncomingForMeToUart = false;

  // Initialize a command state to process the buffer passed from the host.
  EmberCommandState ciStateValue;
  emberInitializeCommandState(&ciStateValue);
  bool ret = emberRunAsciiCommandInterpreter(&ciStateValue,
                                             emberCommandTable,
                                             emberCommandErrorHandler,
                                             message,
                                             messageLength);

  if (ret) {
    emberAfAppPrintln("Command success");
  } else {
    emberAfAppPrintln("Error in processing command on NCP");
  }
}
#endif
