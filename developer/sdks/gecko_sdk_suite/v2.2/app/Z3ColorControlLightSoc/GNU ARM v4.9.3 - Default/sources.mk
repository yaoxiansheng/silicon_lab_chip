################################################################################
# Automatically-generated file. Do not edit!
################################################################################

S79_SRCS := 
OBJ_SRCS := 
S_SRCS := 
ASM_SRCS := 
C_SRCS := 
S_UPPER_SRCS := 
O_SRCS := 
EXECUTABLES := 
OBJS := 
C_DEPS := 

# Every subdirectory with source files must be described here
SUBDIRS := \
. \
adc/EFR32 \
address-table \
antenna-stub \
basic \
bulb-pwm-cli \
bulb-pwm-driver/EFR32 \
bulb-ui \
color-control-server \
configuration-server \
debug-basic-library/EFR32 \
debug-jtag/EFR32 \
eeprom \
efr32 \
ember-minimal-printf \
emdrv \
emlib \
end-device-support \
external-generated-files \
find-and-bind-target \
groups-server \
hal-library/EFR32 \
identify \
led-rgb-pwm \
level-control \
manufacturing-library-cli \
manufacturing-library-ota \
network-steering \
on-off \
ota-bootload \
ota-client \
ota-client-policy \
ota-common \
ota-storage-common \
ota-storage-simple \
ota-storage-simple-eeprom \
reporting \
scan-dispatch \
scenes \
serial/EFR32 \
serial \
sim-eeprom2/EFR32 \
simple-main \
update-tc-link-key \

