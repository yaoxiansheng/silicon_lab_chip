/***************************************************************************//**
 * @file mic.h
 * @brief Driver for the SPV1840LR5H-B MEMS Microphone
 * @version 5.3.3
 *******************************************************************************
 * # License
 * <b>Copyright 2017 Silicon Laboratories, Inc. http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silicon Labs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/

#ifndef MIC_H
#define MIC_H

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include "mic_config.h"

/**************************************************************************//**
* @addtogroup TBSense_BSP
* @{
******************************************************************************/

/***************************************************************************//**
 * @addtogroup Mic
 * @{
 ******************************************************************************/

/** @cond DO_NOT_INCLUDE_WITH_DOXYGEN */
#define MIC_ADC_ACQ_TIME      MIC_CONFIG_ADC_ACQ_TIME    /**< ADC acquisition time  */
#define MIC_ADC_CLOCK_FREQ    MIC_CONFIG_ADC_CLOCK_FREQ  /**< ADC clock frequency   */
/** @endcond */

#define MIC_SAMPLE_BUFFER_SIZE      (112)                                          /**< DMA readout buffer size  */
#define MIC_SEND_BUFFER_SIZE        (224)                                          /**< Buffer size used to send data via BLE */
#define PROCESS_DATA_FACTOR         (2)                                            /**< Factor required to select 16bits data from 32bits */
#define PROCESS_DATA_BUFFER_SIZE    (MIC_SAMPLE_BUFFER_SIZE / PROCESS_DATA_FACTOR) /**< Buffer size required for selecting 16bits data from 32bits */

/***************************************************************************************************
 * Type Definitions
 **************************************************************************************************/
typedef struct {
  int16_t buffer0[MIC_SAMPLE_BUFFER_SIZE];              /**< DMA data Buffer0 */
  int16_t buffer1[MIC_SAMPLE_BUFFER_SIZE];              /**< DMA data Buffer1 */
}audio_data_buff_t;

#define EXT_SIGNAL_BUFFER0_READY  (1 << 2)  /**< External signal - Buffer0 ready for data processing */
#define EXT_SIGNAL_BUFFER1_READY  (1 << 3)  /**< External signal - Buffer1 ready for data processing */

/**************************************************************************//**
* @name Error Codes
* @{
******************************************************************************/
#define MIC_OK                0                          /**< No errors */
/**@}*/

uint32_t  MIC_init           (uint32_t fs, audio_data_buff_t *buffer);
void      MIC_deInit         (void);

int16_t *MIC_getSampleBuffer(void);
int16_t *MIC_getSampleBuffer0(void);
int16_t *MIC_getSampleBuffer1(void);

/** @} */
/** @} */

#endif // MIC_H
