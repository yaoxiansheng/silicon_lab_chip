/**************************************************************************//**
 * Copyright 2017 Silicon Laboratories, Inc.
 *
 * @section License
 * LICENSE_PLACEHOLDER
 *****************************************************************************/

/**************************************************************************//**
 * @file   main.c
 * @brief  A minimal project structure, used as a starting point for custom
 *         Dynamic-Multiprotocol applications. The project has the basic
 *         functionality enabling peripheral connectivity, without GATT
 *         services. It runs on top of Micrium OS RTOS and multiprotocol RAIL.
 *****************************************************************************/

#include "bsp/siliconlabs/generic/include/bsp_os.h"
#include "bsp/siliconlabs/generic/include/bsp_cpu.h"

#include <common/include/common.h>
#include <common/include/lib_def.h>
#include <common/include/rtos_utils.h>
#include <common/include/toolchains.h>
#include <common/include/rtos_prio.h>
#include <cpu/include/cpu.h>
#include <kernel/include/os.h>

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#include "hal-config.h"
#include "init_mcu.h"
#include "init_board.h"
#include "init_app.h"
#include "ble-configuration.h"
#include "board_features.h"

#include "bsphalconfig.h"
#include "bsp.h"

#include "em_cmu.h"
#include "sleep.h"

#include "rtos_bluetooth.h"
#include "rtos_gecko.h"
#include "gatt_db.h"

#include "rail.h"
#include "rail_types.h"
#include "rail_config.h"

/*
 *********************************************************************************************************
 *********************************************************************************************************
 *                                             LOCAL DEFINES
 *********************************************************************************************************
 *********************************************************************************************************
 */

// Ex Main Start task
#define EX_MAIN_START_TASK_PRIO           21u
#define EX_MAIN_START_TASK_STK_SIZE       512u
static CPU_STK exMainStartTaskStk[EX_MAIN_START_TASK_STK_SIZE];
static OS_TCB  exMainStartTaskTCB;
static void    exMainStartTask(void *p_arg);

#define APP_CFG_TASK_BLUETOOTH_LL_PRIO    3u
#define APP_CFG_TASK_BLUETOOTH_STACK_PRIO 4u

// Bluetooth Application task
#define BLUETOOTH_APP_TASK_PRIO           5u
#define BLUETOOTH_APP_TASK_STACK_SIZE     (1024 / sizeof(CPU_STK))
static CPU_STK bluetoothAppTaskStk[BLUETOOTH_APP_TASK_STACK_SIZE];
static OS_TCB  bluetoothAppTaskTCB;
static void    bluetoothAppTask(void *p_arg);

// Proprietary Application task
#define PROPRIETARY_APP_TASK_PRIO         6u
#define PROPRIETARY_APP_TASK_STACK_SIZE   (2000 / sizeof(CPU_STK))
static CPU_STK proprietaryAppTaskStk[PROPRIETARY_APP_TASK_STACK_SIZE];
static OS_TCB  proprietaryAppTaskTCB;
static void    proprietaryAppTask(void *p_arg);

// Micrium OS hooks
static void idleHook(void);
static void setupHooks(void);

// Proprietary radio
static RAIL_Handle_t railHandle = NULL;
static RAILSched_Config_t railSchedState;
static void radioEventHandler(RAIL_Handle_t railHandle,
                              RAIL_Events_t events);
static RAIL_Config_t railCfg = {
  .eventsCallback = &radioEventHandler,
  .protocol = NULL,
  .scheduler = &railSchedState,
};

// Bluetooth stack
#define MAX_CONNECTIONS 1
uint8_t bluetooth_stack_heap[DEFAULT_BLUETOOTH_HEAP(MAX_CONNECTIONS)];
/* Configuration parameters (see gecko_configuration.h) */
static const gecko_configuration_t bluetooth_config =
{
  .config_flags = GECKO_CONFIG_FLAG_RTOS,
  .sleep.flags = 0,
  .bluetooth.max_connections = MAX_CONNECTIONS,
  .bluetooth.heap = bluetooth_stack_heap,
  .bluetooth.heap_size = sizeof(bluetooth_stack_heap),
  .gattdb = &bg_gattdb_data,
  .ota.flags = 0,
  .ota.device_name_len = 3,
  .ota.device_name_ptr = "OTA",
  .scheduler_callback = BluetoothLLCallback,
  .stack_schedule_callback = BluetoothUpdate,
#if (HAL_PA_ENABLE) && defined(FEATURE_PA_HIGH_POWER)
  .pa.config_enable = 1, // Enable high power PA
  .pa.input = GECKO_RADIO_PA_INPUT_VBAT, // Configure PA input to VBAT
#endif // (HAL_PA_ENABLE) && defined(FEATURE_PA_HIGH_POWER)
  .mbedtls.flags = GECKO_MBEDTLS_FLAGS_NO_MBEDTLS_DEVICE_INIT,
  .mbedtls.dev_number = 0,
};
static uint8_t boot_to_dfu = 0;

/**************************************************************************//**
 * Main.
 *
 * @returns Returns 1.
 *
 * This is the standard entry point for C applications. It is assumed that your
 * code will call main() once you have performed all necessary initialization.
 *****************************************************************************/
int main(void)
{
  RTOS_ERR err;

  // Initialize device.
  initMcu();
  // Initialize board.
  initBoard();
  // Initialize application.
  initApp();
  // Initialize CPU and make all interrupts Kernel Aware.
  BSP_CPUInit();
  // Setup a 1024 Hz tick instead of the default 1000 Hz. This improves
  // accuracy when using dynamic tick which runs of the RTCC at 32768 Hz.
  OS_TASK_CFG tickTaskCfg = {
    .StkBasePtr = DEF_NULL,
    .StkSize    = 256u,
    .Prio       = KERNEL_TICK_TASK_PRIO_DFLT,
    .RateHz     = 1024u
  };
  OS_ConfigureTickTask(&tickTaskCfg);
  // Initialize the Kernel.
  OSInit(&err);
  APP_RTOS_ASSERT_DBG((RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE), 1);
  // Initialize Kernel tick source.
  BSP_TickInit();
  // Setup the Micrium OS hooks.
  setupHooks();
  // Initialise Bluetooth stack.
  gecko_init(&bluetooth_config);
  gecko_init_multiprotocol(NULL);
  // Create the Ex Main Start task
  OSTaskCreate(&exMainStartTaskTCB,
               "Ex Main Start Task",
               exMainStartTask,
               DEF_NULL,
               EX_MAIN_START_TASK_PRIO,
               &exMainStartTaskStk[0],
               (EX_MAIN_START_TASK_STK_SIZE / 10u),
               EX_MAIN_START_TASK_STK_SIZE,
               0u,
               0u,
               DEF_NULL,
               (OS_OPT_TASK_STK_CLR),
               &err);
  APP_RTOS_ASSERT_DBG((RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE), 1);
  // Start the kernel.
  OSStart(&err);
  APP_RTOS_ASSERT_DBG((RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE), 1);

  return (1);
}

/***************************************************************************//**
 * This is the idle hook.
 *
 * This will be called by the Micrium OS idle task when there is no other task
 * ready to run. We just enter the lowest possible energy mode.
 ******************************************************************************/
void SleepAndSyncProtimer(void);
static void idleHook(void)
{
  // Put MCU in the lowest sleep mode available, usually EM2.
  SleepAndSyncProtimer();
}

/***************************************************************************//**
 * Setup the Micrium OS hooks.
 *
 * Setup the Micrium OS hooks. We are only using the idle task hook in this
 * example. See the Mcirium OS documentation for more information on the other
 * available hooks.
 ******************************************************************************/
static void setupHooks(void)
{
  CPU_SR_ALLOC();
  CPU_CRITICAL_ENTER();
  // Don't allow EM3, since we use LF clocks.
  SLEEP_SleepBlockBegin(sleepEM3);
  OS_AppIdleTaskHookPtr = idleHook;
  CPU_CRITICAL_EXIT();
}

/**************************************************************************//**
 * Task to initialise Bluetooth and Proprietary tasks.
 *
 * @param p_arg Pointer to an optional data area which can pass parameters to
 *              the task when the task executes.
 *
 * This is the task that will be called by from main() when all services are
 * initialized successfully.
 *****************************************************************************/
static void exMainStartTask(void *p_arg)
{
  PP_UNUSED_PARAM(p_arg);
  RTOS_ERR err;

#if (OS_CFG_STAT_TASK_EN == DEF_ENABLED)
  // Initialize CPU Usage.
  OSStatTaskCPUUsageInit(&err);
  APP_RTOS_ASSERT_DBG((RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE),; );
#endif

#ifdef CPU_CFG_INT_DIS_MEAS_EN
  // Initialize interrupts disabled measurement.
  CPU_IntDisMeasMaxCurReset();
#endif

  // Create Bluetooth Link Layer and stack tasks
  bluetooth_start_task(APP_CFG_TASK_BLUETOOTH_LL_PRIO,
                       APP_CFG_TASK_BLUETOOTH_STACK_PRIO);

  // Create the Bluetooth Application task
  OSTaskCreate(&bluetoothAppTaskTCB,
               "Bluetooth App Task",
               bluetoothAppTask,
               0u,
               BLUETOOTH_APP_TASK_PRIO,
               &bluetoothAppTaskStk[0u],
               (BLUETOOTH_APP_TASK_STACK_SIZE / 10u),
               BLUETOOTH_APP_TASK_STACK_SIZE,
               0u,
               0u,
               0u,
               (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
               &err);

  // Create the Proprietary Application task
  OSTaskCreate(&proprietaryAppTaskTCB,
               "Proprietary App Task",
               proprietaryAppTask,
               0u,
               PROPRIETARY_APP_TASK_PRIO,
               &proprietaryAppTaskStk[0u],
               (PROPRIETARY_APP_TASK_STACK_SIZE / 10u),
               PROPRIETARY_APP_TASK_STACK_SIZE,
               0u,
               0u,
               0u,
               (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
               &err);

  // Done starting everyone else so let's exit.
  OSTaskDel((OS_TCB *)0, &err);
}

/**************************************************************************//**
 * Bluetooth Application task.
 *
 * @param p_arg Pointer to an optional data area which can pass parameters to
 *              the task when the task executes.
 *
 * This is a minimal Bluetooth Application task that starts advertising after
 * boot and supports OTA upgrade feature.
 *****************************************************************************/
static void bluetoothAppTask(void *p_arg)
{
  PP_UNUSED_PARAM(p_arg);
  RTOS_ERR err;

  while (DEF_TRUE) {
    OSFlagPend(&bluetooth_event_flags,
               (OS_FLAGS)BLUETOOTH_EVENT_FLAG_EVT_WAITING,
               (OS_TICK)0,
               OS_OPT_PEND_BLOCKING       \
               + OS_OPT_PEND_FLAG_SET_ANY \
               + OS_OPT_PEND_FLAG_CONSUME,
               NULL,
               &err);

    // Handle stack events
    switch (BGLIB_MSG_ID(bluetooth_evt->header)) {
      // This boot event is generated when the system boots up after reset.
      // Do not call any stack commands before receiving the boot event.
      // Here the system is set to start advertising immediately after boot
      // procedure.
      case gecko_evt_system_boot_id:
        // Set advertising parameters. 100ms advertisement interval.
        // The first parameter is advertising set handle
        // The next two parameters are minimum and maximum advertising
        // interval, both in units of (milliseconds * 1.6).
        // The last two parameters are duration and maxevents left as default.
        gecko_cmd_le_gap_set_advertise_timing(0, 160, 160, 0, 0);
        // Start general advertising and enable connections.
        gecko_cmd_le_gap_start_advertising(0,
                                           le_gap_general_discoverable,
                                           le_gap_connectable_scannable);
        break;

      case gecko_evt_le_connection_closed_id:
        // Check if need to boot to dfu mode.
        if (boot_to_dfu) {
          // Enter to DFU OTA mode.
          gecko_cmd_system_reset(2);
        } else {
          // Restart advertising after client has disconnected.
          gecko_cmd_le_gap_start_advertising(0,
                                             le_gap_general_discoverable,
                                             le_gap_connectable_scannable);
        }
        break;

      // Events related to OTA upgrading
      // Check if the user-type OTA Control Characteristic was written.
      // If ota_control was written, boot the device into Device Firmware
      // Upgrade (DFU) mode.
      case gecko_evt_gatt_server_user_write_request_id:
        if (bluetooth_evt->data.evt_gatt_server_user_write_request.characteristic == gattdb_ota_control) {
          // Set flag to enter to OTA mode.
          boot_to_dfu = 1;
          // Send response to Write Request.
          gecko_cmd_gatt_server_send_user_write_response(
            bluetooth_evt->data.evt_gatt_server_user_write_request.connection,
            gattdb_ota_control,
            bg_err_success);
          // Close connection to enter to DFU OTA mode.
          gecko_cmd_le_connection_close(bluetooth_evt->data.evt_gatt_server_user_write_request.connection);
        }
        break;

      default:
        break;
    }

    OSFlagPost(&bluetooth_event_flags,
               (OS_FLAGS)BLUETOOTH_EVENT_FLAG_EVT_HANDLED,
               OS_OPT_POST_FLAG_SET,
               &err);
  }
}

/**************************************************************************//**
 * Proprietary Application task.
 *
 * @param p_arg Pointer to an optional data area which can pass parameters to
 *              the task when the task executes.
 *
 * This is a minimal Proprietary Application task that only configures the
 * radio.
 *****************************************************************************/
static void proprietaryAppTask(void *p_arg)
{
  PP_UNUSED_PARAM(p_arg);

  // Create each RAIL handle with their own configuration structures
  railHandle = RAIL_Init(&railCfg, NULL);
  // Configure radio according to the generated radio settings
  RAIL_TxPowerConfig_t railTxPowerConfig = {
#if HAL_PA_2P4_LOWPOWER
    .mode = RAIL_TX_POWER_MODE_2P4_LP,
#else
    .mode = RAIL_TX_POWER_MODE_2P4_HP,
#endif
    .voltage = HAL_PA_VOLTAGE,
    .rampTime = HAL_PA_RAMP,
  };
  if (channelConfigs[0]->configs[0].baseFrequency < 1000000UL) {
    // Use the Sub-GHz PA if required
    railTxPowerConfig.mode = RAIL_TX_POWER_MODE_SUBGIG;
  }
  if (RAIL_ConfigTxPower(railHandle, &railTxPowerConfig) != RAIL_STATUS_NO_ERROR) {
    while (1) ;
  }
  // We must reapply the Tx power after changing the PA above
  RAIL_SetTxPower(railHandle, HAL_PA_POWER);
  RAIL_ConfigChannels(railHandle, channelConfigs[0], NULL);
  // Configure the most useful callbacks plus catch a few errors
  RAIL_ConfigEvents(railHandle,
                    RAIL_EVENTS_ALL,
                    RAIL_EVENTS_ALL);

  while (DEF_TRUE) {
    //
    // Put your code here!
    //
  }
}

static void radioEventHandler(RAIL_Handle_t railHandle,
                              RAIL_Events_t events)
{
  PP_UNUSED_PARAM(railHandle);
  PP_UNUSED_PARAM(events);
}
