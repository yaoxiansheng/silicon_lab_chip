#ISD afv6
# ISD version: 3.4.2836

# Application configuration
appId: ble
architecture: efr32~family[B]~performance[P]~radio[232]~flash[256K]~temp[G]~package[M]~pins[48]+BRD4100A+iar
deviceName: soc-switched-multiprotocol-joining-device
generationDirectory: PATH(ISC_RELATIVE):.

# UI customization
hiddenLayoutElements: plugins,callbacks,other

# Plugin configuration
appPlugin: mpsi=true
appPlugin: mpsi_ble_transport_server=true
appPlugin: psstore=true
# Plugins done by code generator so no need for them in AppBuilder generation
appPlugin: mpsi-storage=false
appPlugin: slot-manager=false
appPlugin: stack_bridge=false
# Plugin that is not needed for the joining device
appPlugin: mpsi-ipc=false

# Setup configurations
{setupId:hwConfig
featureLevel=1
active=true
}

{setupId:bleGattConfigurator
<?xml version="1.0" encoding="UTF-8"?><project>
  <gatt generic_attribute_service="true" header="gatt_db.h" name="Custom BLE GATT" out="gatt_db.c" prefix="gattdb_">
    <service advertise="false" name="Generic Access" requirement="mandatory" sourceId="org.bluetooth.service.generic_access" type="primary" uuid="1800">
      <informativeText>Abstract: The generic_access service contains generic information about the device. All available Characteristics are readonly. </informativeText>
      <characteristic id="device_name" name="Device Name" sourceId="org.bluetooth.characteristic.gap.device_name" uuid="2a00">
        <informativeText/>
        <value length="8" type="utf-8" variable_length="false">SLJD0000</value>
        <properties read="true" read_requirement="mandatory" write="true" write_requirement="optional"/>
      </characteristic>
      <characteristic name="Appearance" sourceId="org.bluetooth.characteristic.gap.appearance" uuid="2a01">
        <informativeText>Abstract: The external appearance of this device. The values are composed of a category (10-bits) and sub-categories (6-bits). </informativeText>
        <value length="2" type="hex" variable_length="false">0003</value>
        <properties const="true" const_requirement="optional" read="true" read_requirement="optional"/>
      </characteristic>
    </service>
    <service advertise="false" name="Device Information" requirement="mandatory" type="primary" uuid="180A">
      <informativeText/>
      <characteristic name="Manufacturer Name String" uuid="2A29">
        <informativeText>Abstract: The value of this characteristic is a UTF-8 string representing the name of the manufacturer of the device. </informativeText>
        <value length="1" type="utf-8" variable_length="false">Silicon Labs</value>
        <properties const="true" const_requirement="mandatory" read="true" read_requirement="mandatory"/>
      </characteristic>
    </service>
    <service advertise="false" id="mpsi" name="Silicon Labs MPSI Service" requirement="mandatory" sourceId="com.silabs.service.mpsi" type="primary" uuid="e52f5fda-bc84-4b88-a99e-001924b65598">
      <informativeText>Abstract:  The Silicon Labs Multi Protocol Stack Interface (MPSI) Service enables communication between Silicon Labs' stacks.  </informativeText>
      <characteristic id="mpsi_control_in" name="Silicon Labs MPSI Control In" sourceId="com.silabs.characteristic.mpsi_control_in" uuid="D5C082D7-5959-431D-9213-94F570E73851">
        <informativeText>Abstract:  Silicon Labs MPSI Control In.  </informativeText>
        <value length="5" type="user" variable_length="true">0x00</value>
        <properties write="true" write_requirement="mandatory"/>
      </characteristic>
      <characteristic id="mpsi_message_in" name="Silicon Labs MPSI Message In" sourceId="com.silabs.characteristic.mpsi_message_in" uuid="1E532E88-610F-42B2-A07E-5505E5329605">
        <informativeText>Abstract:  Silicon Labs MPSI Message In.  </informativeText>
        <value length="255" type="user" variable_length="true">0x00</value>
        <properties write="true" write_requirement="mandatory"/>
      </characteristic>
      <characteristic id="mpsi_control_out" name="Silicon Labs MPSI Control Out" sourceId="com.silabs.characteristic.mpsi_control_out" uuid="86067CA8-0477-41E9-815F-AE20FBEADF54">
        <informativeText>Abstract:  Silicon Labs MPSI Control Out.  </informativeText>
        <value length="5" type="user" variable_length="true">0x00</value>
        <properties indicate="true" indicate_requirement="mandatory"/>
      </characteristic>
      <characteristic id="mpsi_message_out" name="Silicon Labs MPSI Message Out" sourceId="com.silabs.characteristic.mpsi_message_out" uuid="98732EC1-0B34-4D03-AEA1-0808104499A8">
        <informativeText>Abstract:  Silicon Labs MPSI Message Out.  </informativeText>
        <value length="54" type="user" variable_length="true">0x00</value>
        <properties read="true" read_requirement="mandatory"/>
      </characteristic>
    </service>
    <service advertise="true" id="mpsi_joining_device" name="Silicon Labs MPSI Joining Device" requirement="mandatory" sourceId="com.silabs.service.mpsi_joining_device" type="primary" uuid="5B44155E-2C17-4D7B-8508-1B900C32D524">
      <informativeText>Abstract:  The MPSI Joining Device Service contains information about the Role (Joining Device) of the Device.  </informativeText>
      <include id="mpsi" sourceId="com.silabs.service.mpsi"/>
    </service>
    <service advertise="false" name="Silicon Labs OTA" requirement="mandatory" type="primary" uuid="1d14d6ee-fd63-4fa1-bfa4-8f47b42119f0">
      <informativeText/>
      <characteristic id="ota_control" name="OTA control" uuid="f7bf3564-fb6d-4e53-88a4-5e37e0326063">
        <informativeText>Custom characteristic</informativeText>
        <value length="5" type="user" variable_length="true">0x00</value>
        <properties write="true" write_requirement="mandatory"/>
      </characteristic>
      <characteristic id="ota_data" name="OTA Data" sourceId="com.silabs.characteristic.ota_data" uuid="984227F3-34FC-4045-A5D0-2C581F81A153">
        <informativeText>Abstract: Silicon Labs OTA Data. </informativeText>
        <value length="255" type="user" variable_length="false"/>
        <properties write="true" write_no_response="true" write_no_response_requirement="mandatory" write_requirement="mandatory"/>
      </characteristic>
    </service>
  </gatt>
</project>
}

{setupId:callbackConfiguration
bleMpsiTransportLongMessageReceived:false
bleMpsiTransportLongMessageReceptionCrcFail:false
bleMpsiTransportLongMessageSent:false
bleMpsiTransportLongMessageSendingCrcFail:false
bleMpsiTransportConfirmBonding:false
bleMpsiTransportConfirmPasskey:false
mpsiHandleMessageGetAppsInfo:false
mpsiHandleMessageAppsInfo:false
mpsiHandleMessageBootloadSlot:false
mpsiHandleMessageError:false
mpsiHandleMessageInitiateJoining:false
mpsiHandleMessageGetZigbeeJoiningDeviceInfo:false
mpsiHandleMessageZigbeeJoiningDeviceInfo:false
mpsiHandleMessageSetZigbeeJoiningDeviceInfo:false
mpsiHandleMessageGetZigbeeTrustCenterJoiningCredentials:false
mpsiHandleMessageZigbeeTrustCenterJoiningCredentials:false
mpsiHandleMessageSetZigbeeTrustCenterJoiningCredentials:false
bleMpsiTransportOtaDfuTransactionBegin:false
bleMpsiTransportOtaDfuTransactionFinish:false
bleMpsiTransportOtaDfuDataReceived:false
}

