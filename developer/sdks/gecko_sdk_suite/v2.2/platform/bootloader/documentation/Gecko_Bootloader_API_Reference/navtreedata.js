var NAVTREE =
[
  [ "Gecko Bootloader API Reference", "index.html", [
    [ "Silicon Labs Gecko Bootloader", "index.html", null ],
    [ "API Documentation", "modules.html", "modules" ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ]
    ] ],
    [ "silabs.com", "^http://www.silabs.com", null ]
  ] ]
];

var NAVTREEINDEX =
[
"application__properties_8h.html",
"group__CommonInterface.html#ga8399dd23ea8f5e3232359386c37f4c63",
"group__Flash.html",
"group__SpiflashConfigs.html#ga947898d96435f632f1c2227620707d03",
"group__XmodemError.html#gab5478113a61fb3d72671e1c38f302e8b"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';