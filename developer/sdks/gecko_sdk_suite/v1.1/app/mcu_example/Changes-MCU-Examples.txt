================ Revision history ============================================
5.2.2:
  SLSTK3401A_EFM32PG
    - Fixed bug in FreeRTOS Demo example. When configuring the demo to use low
      power the example would enter EM3 and the tick interrupt would not wakeup
      the MCU.

5.2.1:
  EFM32GG_DK3750:
    - Fixed usbdtouch example. Changed FAT partitioning style from SFD to FDISK.
      This fixes disk mount problem observed when using Windows 10 host.
  EFM32GG_STK3700
    - Added MicriumOS Blink example.
    - Added MicriumOS USB device hid mouse example.
    - Added MicriumOS USB host MSC example.
  EFM32WG_STK3800
    - Added usb_isolation, rs232_isolation and rs485_isolation examples.
  SLSTK3400A_EFM32HG
    - Added the gterm example.
  SLSTK3401A_EFM32PG
    - Added MicriumOS Blink example.
  SLSTK3402A_EFM32PG12
    - Added MicriumOS Blink example.
  SLSTK3701A_EFM32GG11
   - Added SLSTK3701A_EFM32GG11 kit examples.

5.1.3:
  SLSTK3402A_EFM32PG12
    - Added Simplicity Studio support.

5.1.1:
  SLSTK3400A_EFM32HG
    - Fixed incorrect push button pin in emode example.

5.1.0:
  SLSTK3401A_EFM32PG
    - Added uC/OS-III Glib example.

5.0.0:
  EFM32GG_DK3750:
    - Added leuart example.
  EFM32GG_STK3700
    - Added FreeRTOS Demo.
    - Added leuart example.
  EFM32WG_STK3800
    - Added leuart example.
  EFM32ZG_STK3200
    - Added leuart example.
    - Added SPI example.
  EFM32_Gxxx_STK
    - Added leuart example.
  SLSTK3400A_EFM32HG
    - Added USB Mass Storage Device example.
    - Added leuart example.
    - Added SPI example.
  SLSTK3401A_EFM32PG
    - Added FreeRTOS Demo.
    - Added tempdrv example.
    - Added leuart example.
    - Added SPI example.
