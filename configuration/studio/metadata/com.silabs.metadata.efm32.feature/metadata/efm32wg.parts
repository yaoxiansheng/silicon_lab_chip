<?xml version="1.0" encoding="UTF-8"?>
<collection>
  <group parent="mcu.arm.efm32" name="wg">
    <property key="referenceManuals" value="asset://efm32/reference/efm32wg_reference_manual.pdf"/>
    <property key="hardwareConfigData" value="asset://efm32/configurator/efm32wg/EFM32WG"/>
    <property key="keywords" value=""/>
    <property key="memory.FLASH.addr" value="0"/>
    <property key="memory.FLASH.access" value="rx"/>
    <property key="memory.FLASH.pageSize" value="0x800"/>
    <property key="memory.RAM.addr" value="0x20000000"/>
    <property key="memory.RAM.access" value="rwx"/>
    <property key="ARMCore" value="CORTEX_M4F"/>
    <property key="FPU" value="VFP_V4"/>
    <property key="hardware.part.familyType" value="Wonder"/>

    <part name="efm32wg995f256" label="EFM32WG995F256">
      <property key="memory.FLASH.size" value="0x40000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG995.json asset://efm32/debug/wg/EFM32WG995F256_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg995_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg995_errata.pdf"/>
      <property key="hardware.part.featureSet" value="995"/>
      <property key="hardware.part.flash" value="256K"/>
    </part>

    <part name="efm32wg995f128" label="EFM32WG995F128">
      <property key="memory.FLASH.size" value="0x20000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG995.json asset://efm32/debug/wg/EFM32WG995F128_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg995_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg995_errata.pdf"/>
      <property key="hardware.part.featureSet" value="995"/>
      <property key="hardware.part.flash" value="128K"/>
    </part>

    <part name="efm32wg995f64" label="EFM32WG995F64">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG995.json asset://efm32/debug/wg/EFM32WG995F64_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg995_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg995_errata.pdf"/>
      <property key="hardware.part.featureSet" value="995"/>
      <property key="hardware.part.flash" value="64K"/>
    </part>

    <part name="efm32wg990f256" label="EFM32WG990F256">
      <property key="memory.FLASH.size" value="0x40000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG990.json asset://efm32/debug/wg/EFM32WG990F256_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg990_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg990_errata.pdf"/>
      <property key="hardware.part.featureSet" value="990"/>
      <property key="hardware.part.flash" value="256K"/>
    </part>

    <part name="efm32wg990f128" label="EFM32WG990F128">
      <property key="memory.FLASH.size" value="0x20000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG990.json asset://efm32/debug/wg/EFM32WG990F128_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg990_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg990_errata.pdf"/>
      <property key="hardware.part.featureSet" value="990"/>
      <property key="hardware.part.flash" value="128K"/>
    </part>

    <part name="efm32wg990f64" label="EFM32WG990F64">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG990.json asset://efm32/debug/wg/EFM32WG990F64_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg990_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg990_errata.pdf"/>
      <property key="hardware.part.featureSet" value="990"/>
      <property key="hardware.part.flash" value="64K"/>
    </part>

    <part name="efm32wg980f256" label="EFM32WG980F256">
      <property key="memory.FLASH.size" value="0x40000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG980.json asset://efm32/debug/wg/EFM32WG980F256_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg980_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg980_errata.pdf"/>
      <property key="hardware.part.featureSet" value="980"/>
      <property key="hardware.part.flash" value="256K"/>
    </part>

    <part name="efm32wg980f128" label="EFM32WG980F128">
      <property key="memory.FLASH.size" value="0x20000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG980.json asset://efm32/debug/wg/EFM32WG980F128_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg980_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg980_errata.pdf"/>
      <property key="hardware.part.featureSet" value="980"/>
      <property key="hardware.part.flash" value="128K"/>
    </part>

    <part name="efm32wg980f64" label="EFM32WG980F64">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG980.json asset://efm32/debug/wg/EFM32WG980F64_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg980_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg980_errata.pdf"/>
      <property key="hardware.part.featureSet" value="980"/>
      <property key="hardware.part.flash" value="64K"/>
    </part>

    <part name="efm32wg942f256" label="EFM32WG942F256">
      <property key="memory.FLASH.size" value="0x40000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG942.json asset://efm32/debug/wg/EFM32WG942F256_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg942_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg942_errata.pdf"/>
      <property key="hardware.part.featureSet" value="942"/>
      <property key="hardware.part.flash" value="256K"/>
    </part>

    <part name="efm32wg942f128" label="EFM32WG942F128">
      <property key="memory.FLASH.size" value="0x20000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG942.json asset://efm32/debug/wg/EFM32WG942F128_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg942_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg942_errata.pdf"/>
      <property key="hardware.part.featureSet" value="942"/>
      <property key="hardware.part.flash" value="128K"/>
    </part>

    <part name="efm32wg942f64" label="EFM32WG942F64">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG942.json asset://efm32/debug/wg/EFM32WG942F64_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg942_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg942_errata.pdf"/>
      <property key="hardware.part.featureSet" value="942"/>
      <property key="hardware.part.flash" value="64K"/>
    </part>

    <part name="efm32wg940f256" label="EFM32WG940F256">
      <property key="memory.FLASH.size" value="0x40000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG940.json asset://efm32/debug/wg/EFM32WG940F256_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg940_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg940_errata.pdf"/>
      <property key="hardware.part.featureSet" value="940"/>
      <property key="hardware.part.flash" value="256K"/>
    </part>

    <part name="efm32wg940f128" label="EFM32WG940F128">
      <property key="memory.FLASH.size" value="0x20000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG940.json asset://efm32/debug/wg/EFM32WG940F128_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg940_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg940_errata.pdf"/>
      <property key="hardware.part.featureSet" value="940"/>
      <property key="hardware.part.flash" value="128K"/>
    </part>

    <part name="efm32wg940f64" label="EFM32WG940F64">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG940.json asset://efm32/debug/wg/EFM32WG940F64_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg940_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg940_errata.pdf"/>
      <property key="hardware.part.featureSet" value="940"/>
      <property key="hardware.part.flash" value="64K"/>
    </part>

    <part name="efm32wg900f256" label="EFM32WG900F256">
      <property key="memory.FLASH.size" value="0x40000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG900.json asset://efm32/debug/wg/EFM32WG900F256_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg900_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg900_errata.pdf"/>
      <property key="hardware.part.featureSet" value="900"/>
      <property key="hardware.part.flash" value="256K"/>
    </part>

    <part name="efm32wg895f256" label="EFM32WG895F256">
      <property key="memory.FLASH.size" value="0x40000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG895.json asset://efm32/debug/wg/EFM32WG895F256_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg895_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg895_errata.pdf"/>
      <property key="hardware.part.featureSet" value="895"/>
      <property key="hardware.part.flash" value="256K"/>
    </part>

    <part name="efm32wg895f128" label="EFM32WG895F128">
      <property key="memory.FLASH.size" value="0x20000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG895.json asset://efm32/debug/wg/EFM32WG895F128_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg895_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg895_errata.pdf"/>
      <property key="hardware.part.featureSet" value="895"/>
      <property key="hardware.part.flash" value="128K"/>
    </part>

    <part name="efm32wg895f64" label="EFM32WG895F64">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG895.json asset://efm32/debug/wg/EFM32WG895F64_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg895_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg895_errata.pdf"/>
      <property key="hardware.part.featureSet" value="895"/>
      <property key="hardware.part.flash" value="64K"/>
    </part>

    <part name="efm32wg890f256" label="EFM32WG890F256">
      <property key="memory.FLASH.size" value="0x40000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG890.json asset://efm32/debug/wg/EFM32WG890F256_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg890_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg890_errata.pdf"/>
      <property key="hardware.part.featureSet" value="890"/>
      <property key="hardware.part.flash" value="256K"/>
    </part>

    <part name="efm32wg890f128" label="EFM32WG890F128">
      <property key="memory.FLASH.size" value="0x20000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG890.json asset://efm32/debug/wg/EFM32WG890F128_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg890_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg890_errata.pdf"/>
      <property key="hardware.part.featureSet" value="890"/>
      <property key="hardware.part.flash" value="128K"/>
    </part>

    <part name="efm32wg890f64" label="EFM32WG890F64">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG890.json asset://efm32/debug/wg/EFM32WG890F64_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg890_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg890_errata.pdf"/>
      <property key="hardware.part.featureSet" value="890"/>
      <property key="hardware.part.flash" value="64K"/>
    </part>

    <part name="efm32wg880f256" label="EFM32WG880F256">
      <property key="memory.FLASH.size" value="0x40000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG880.json asset://efm32/debug/wg/EFM32WG880F256_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg880_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg880_errata.pdf"/>
      <property key="hardware.part.featureSet" value="880"/>
      <property key="hardware.part.flash" value="256K"/>
    </part>

    <part name="efm32wg880f128" label="EFM32WG880F128">
      <property key="memory.FLASH.size" value="0x20000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG880.json asset://efm32/debug/wg/EFM32WG880F128_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg880_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg880_errata.pdf"/>
      <property key="hardware.part.featureSet" value="880"/>
      <property key="hardware.part.flash" value="128K"/>
    </part>

    <part name="efm32wg880f64" label="EFM32WG880F64">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG880.json asset://efm32/debug/wg/EFM32WG880F64_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg880_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg880_errata.pdf"/>
      <property key="hardware.part.featureSet" value="880"/>
      <property key="hardware.part.flash" value="64K"/>
    </part>

    <part name="efm32wg842f256" label="EFM32WG842F256">
      <property key="memory.FLASH.size" value="0x40000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG842.json asset://efm32/debug/wg/EFM32WG842F256_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg842_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg842_errata.pdf"/>
      <property key="hardware.part.featureSet" value="842"/>
      <property key="hardware.part.flash" value="256K"/>
    </part>

    <part name="efm32wg842f128" label="EFM32WG842F128">
      <property key="memory.FLASH.size" value="0x20000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG842.json asset://efm32/debug/wg/EFM32WG842F128_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg842_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg842_errata.pdf"/>
      <property key="hardware.part.featureSet" value="842"/>
      <property key="hardware.part.flash" value="128K"/>
    </part>

    <part name="efm32wg842f64" label="EFM32WG842F64">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG842.json asset://efm32/debug/wg/EFM32WG842F64_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg842_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg842_errata.pdf"/>
      <property key="hardware.part.featureSet" value="842"/>
      <property key="hardware.part.flash" value="64K"/>
    </part>

    <part name="efm32wg840f256" label="EFM32WG840F256">
      <property key="memory.FLASH.size" value="0x40000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG840.json asset://efm32/debug/wg/EFM32WG840F256_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg840_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg840_errata.pdf"/>
      <property key="hardware.part.featureSet" value="840"/>
      <property key="hardware.part.flash" value="256K"/>
    </part>

    <part name="efm32wg840f128" label="EFM32WG840F128">
      <property key="memory.FLASH.size" value="0x20000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG840.json asset://efm32/debug/wg/EFM32WG840F128_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg840_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg840_errata.pdf"/>
      <property key="hardware.part.featureSet" value="840"/>
      <property key="hardware.part.flash" value="128K"/>
    </part>

    <part name="efm32wg840f64" label="EFM32WG840F64">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG840.json asset://efm32/debug/wg/EFM32WG840F64_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg840_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg840_errata.pdf"/>
      <property key="hardware.part.featureSet" value="840"/>
      <property key="hardware.part.flash" value="64K"/>
    </part>

    <part name="efm32wg395f256" label="EFM32WG395F256">
      <property key="memory.FLASH.size" value="0x40000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG395.json asset://efm32/debug/wg/EFM32WG395F256_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg395_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg395_errata.pdf"/>
      <property key="hardware.part.featureSet" value="395"/>
      <property key="hardware.part.flash" value="256K"/>
    </part>

    <part name="efm32wg395f128" label="EFM32WG395F128">
      <property key="memory.FLASH.size" value="0x20000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG395.json asset://efm32/debug/wg/EFM32WG395F128_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg395_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg395_errata.pdf"/>
      <property key="hardware.part.featureSet" value="395"/>
      <property key="hardware.part.flash" value="128K"/>
    </part>

    <part name="efm32wg395f64" label="EFM32WG395F64">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG395.json asset://efm32/debug/wg/EFM32WG395F64_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg395_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg395_errata.pdf"/>
      <property key="hardware.part.featureSet" value="395"/>
      <property key="hardware.part.flash" value="64K"/>
    </part>

    <part name="efm32wg390f256" label="EFM32WG390F256">
      <property key="memory.FLASH.size" value="0x40000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG390.json asset://efm32/debug/wg/EFM32WG390F256_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg390_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg390_errata.pdf"/>
      <property key="hardware.part.featureSet" value="390"/>
      <property key="hardware.part.flash" value="256K"/>
    </part>

    <part name="efm32wg390f128" label="EFM32WG390F128">
      <property key="memory.FLASH.size" value="0x20000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG390.json asset://efm32/debug/wg/EFM32WG390F128_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg390_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg390_errata.pdf"/>
      <property key="hardware.part.featureSet" value="390"/>
      <property key="hardware.part.flash" value="128K"/>
    </part>

    <part name="efm32wg390f64" label="EFM32WG390F64">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG390.json asset://efm32/debug/wg/EFM32WG390F64_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg390_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg390_errata.pdf"/>
      <property key="hardware.part.featureSet" value="390"/>
      <property key="hardware.part.flash" value="64K"/>
    </part>

    <part name="efm32wg380f256" label="EFM32WG380F256">
      <property key="memory.FLASH.size" value="0x40000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG380.json asset://efm32/debug/wg/EFM32WG380F256_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg380_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg380_errata.pdf"/>
      <property key="hardware.part.featureSet" value="380"/>
      <property key="hardware.part.flash" value="256K"/>
    </part>

    <part name="efm32wg380f128" label="EFM32WG380F128">
      <property key="memory.FLASH.size" value="0x20000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG380.json asset://efm32/debug/wg/EFM32WG380F128_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg380_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg380_errata.pdf"/>
      <property key="hardware.part.featureSet" value="380"/>
      <property key="hardware.part.flash" value="128K"/>
    </part>

    <part name="efm32wg380f64" label="EFM32WG380F64">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG380.json asset://efm32/debug/wg/EFM32WG380F64_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg380_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg380_errata.pdf"/>
      <property key="hardware.part.featureSet" value="380"/>
      <property key="hardware.part.flash" value="64K"/>
    </part>

    <part name="efm32wg360f256" label="EFM32WG360F256">
      <property key="memory.FLASH.size" value="0x40000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG360.json asset://efm32/debug/wg/EFM32WG360F256_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg360_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg360_errata.pdf"/>
      <property key="hardware.part.featureSet" value="360"/>
      <property key="hardware.part.flash" value="256K"/>
    </part>

    <part name="efm32wg360f128" label="EFM32WG360F128">
      <property key="memory.FLASH.size" value="0x20000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG360.json asset://efm32/debug/wg/EFM32WG360F128_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg360_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg360_errata.pdf"/>
      <property key="hardware.part.featureSet" value="360"/>
      <property key="hardware.part.flash" value="128K"/>
    </part>

    <part name="efm32wg360f64" label="EFM32WG360F64">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG360.json asset://efm32/debug/wg/EFM32WG360F64_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg360_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg360_errata.pdf"/>
      <property key="hardware.part.featureSet" value="360"/>
      <property key="hardware.part.flash" value="64K"/>
    </part>

    <part name="efm32wg332f256" label="EFM32WG332F256">
      <property key="memory.FLASH.size" value="0x40000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG332.json asset://efm32/debug/wg/EFM32WG332F256_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg332_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg332_errata.pdf"/>
      <property key="hardware.part.featureSet" value="332"/>
      <property key="hardware.part.flash" value="256K"/>
    </part>

    <part name="efm32wg332f128" label="EFM32WG332F128">
      <property key="memory.FLASH.size" value="0x20000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG332.json asset://efm32/debug/wg/EFM32WG332F128_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg332_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg332_errata.pdf"/>
      <property key="hardware.part.featureSet" value="332"/>
      <property key="hardware.part.flash" value="128K"/>
    </part>

    <part name="efm32wg332f64" label="EFM32WG332F64">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG332.json asset://efm32/debug/wg/EFM32WG332F64_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg332_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg332_errata.pdf"/>
      <property key="hardware.part.featureSet" value="332"/>
      <property key="hardware.part.flash" value="64K"/>
    </part>

    <part name="efm32wg330f256" label="EFM32WG330F256">
      <property key="memory.FLASH.size" value="0x40000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG330.json asset://efm32/debug/wg/EFM32WG330F256_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg330_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg330_errata.pdf"/>
      <property key="hardware.part.featureSet" value="330"/>
      <property key="hardware.part.flash" value="256K"/>
    </part>

    <part name="efm32wg330f128" label="EFM32WG330F128">
      <property key="memory.FLASH.size" value="0x20000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG330.json asset://efm32/debug/wg/EFM32WG330F128_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg330_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg330_errata.pdf"/>
      <property key="hardware.part.featureSet" value="330"/>
      <property key="hardware.part.flash" value="128K"/>
    </part>

    <part name="efm32wg330f64" label="EFM32WG330F64">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG330.json asset://efm32/debug/wg/EFM32WG330F64_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg330_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg330_errata.pdf"/>
      <property key="hardware.part.featureSet" value="330"/>
      <property key="hardware.part.flash" value="64K"/>
    </part>

    <part name="efm32wg295f256" label="EFM32WG295F256">
      <property key="memory.FLASH.size" value="0x40000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG295.json asset://efm32/debug/wg/EFM32WG295F256_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg295_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg295_errata.pdf"/>
      <property key="hardware.part.featureSet" value="295"/>
      <property key="hardware.part.flash" value="256K"/>
    </part>

    <part name="efm32wg295f128" label="EFM32WG295F128">
      <property key="memory.FLASH.size" value="0x20000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG295.json asset://efm32/debug/wg/EFM32WG295F128_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg295_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg295_errata.pdf"/>
      <property key="hardware.part.featureSet" value="295"/>
      <property key="hardware.part.flash" value="128K"/>
    </part>

    <part name="efm32wg295f64" label="EFM32WG295F64">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG295.json asset://efm32/debug/wg/EFM32WG295F64_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg295_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg295_errata.pdf"/>
      <property key="hardware.part.featureSet" value="295"/>
      <property key="hardware.part.flash" value="64K"/>
    </part>

    <part name="efm32wg290f256" label="EFM32WG290F256">
      <property key="memory.FLASH.size" value="0x40000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG290.json asset://efm32/debug/wg/EFM32WG290F256_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg290_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg290_errata.pdf"/>
      <property key="hardware.part.featureSet" value="290"/>
      <property key="hardware.part.flash" value="256K"/>
    </part>

    <part name="efm32wg290f128" label="EFM32WG290F128">
      <property key="memory.FLASH.size" value="0x20000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG290.json asset://efm32/debug/wg/EFM32WG290F128_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg290_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg290_errata.pdf"/>
      <property key="hardware.part.featureSet" value="290"/>
      <property key="hardware.part.flash" value="128K"/>
    </part>

    <part name="efm32wg290f64" label="EFM32WG290F64">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG290.json asset://efm32/debug/wg/EFM32WG290F64_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg290_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg290_errata.pdf"/>
      <property key="hardware.part.featureSet" value="290"/>
      <property key="hardware.part.flash" value="64K"/>
    </part>

    <part name="efm32wg280f256" label="EFM32WG280F256">
      <property key="memory.FLASH.size" value="0x40000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG280.json asset://efm32/debug/wg/EFM32WG280F256_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg280_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg280_errata.pdf"/>
      <property key="hardware.part.featureSet" value="280"/>
      <property key="hardware.part.flash" value="256K"/>
    </part>

    <part name="efm32wg280f128" label="EFM32WG280F128">
      <property key="memory.FLASH.size" value="0x20000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG280.json asset://efm32/debug/wg/EFM32WG280F128_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg280_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg280_errata.pdf"/>
      <property key="hardware.part.featureSet" value="280"/>
      <property key="hardware.part.flash" value="128K"/>
    </part>

    <part name="efm32wg280f64" label="EFM32WG280F64">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG280.json asset://efm32/debug/wg/EFM32WG280F64_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg280_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg280_errata.pdf"/>
      <property key="hardware.part.featureSet" value="280"/>
      <property key="hardware.part.flash" value="64K"/>
    </part>

    <part name="efm32wg232f256" label="EFM32WG232F256">
      <property key="memory.FLASH.size" value="0x40000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG232.json asset://efm32/debug/wg/EFM32WG232F256_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg232_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg232_errata.pdf"/>
      <property key="hardware.part.featureSet" value="232"/>
      <property key="hardware.part.flash" value="256K"/>
    </part>

    <part name="efm32wg232f128" label="EFM32WG232F128">
      <property key="memory.FLASH.size" value="0x20000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG232.json asset://efm32/debug/wg/EFM32WG232F128_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg232_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg232_errata.pdf"/>
      <property key="hardware.part.featureSet" value="232"/>
      <property key="hardware.part.flash" value="128K"/>
    </part>

    <part name="efm32wg232f64" label="EFM32WG232F64">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG232.json asset://efm32/debug/wg/EFM32WG232F64_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg232_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg232_errata.pdf"/>
      <property key="hardware.part.featureSet" value="232"/>
      <property key="hardware.part.flash" value="64K"/>
    </part>

    <part name="efm32wg230f256" label="EFM32WG230F256">
      <property key="memory.FLASH.size" value="0x40000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG230.json asset://efm32/debug/wg/EFM32WG230F256_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg230_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg230_errata.pdf"/>
      <property key="hardware.part.featureSet" value="230"/>
      <property key="hardware.part.flash" value="256K"/>
    </part>

    <part name="efm32wg230f128" label="EFM32WG230F128">
      <property key="memory.FLASH.size" value="0x20000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG230.json asset://efm32/debug/wg/EFM32WG230F128_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg230_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg230_errata.pdf"/>
      <property key="hardware.part.featureSet" value="230"/>
      <property key="hardware.part.flash" value="128K"/>
    </part>

    <part name="efm32wg230f64" label="EFM32WG230F64">
      <property key="memory.FLASH.size" value="0x10000"/>
      <property key="memory.RAM.size" value="0x8000"/>
      <property key="debugDescriptors" value="asset://efm32/debug/wg/EFM32WG230.json asset://efm32/debug/wg/EFM32WG230F64_core.json"/>
      <property key="dataSheets" value="asset://efm32/datasheet/efm32wg230_datasheet.pdf"/>
      <property key="errata" value="asset://efm32/errata/efm32wg230_errata.pdf"/>
      <property key="hardware.part.featureSet" value="230"/>
      <property key="hardware.part.flash" value="64K"/>
    </part>
  </group>
</collection>
