                    How to enable Code Correlation
                               for the 
                      Simplicity Energy Profiler


 
Code Correlation is the functionality that allows you to profile and view energy
consumption in an application by its functions as it executes in real time. 
Code Correlation also allows you to select a sample point in the Energy Profiler's
graph view and view the source code line that was executed at that time by the
processor. This will allow a software developer to gain insight into which parts
of the code executed spends the most energy. This insight will enable a software
developer to both find bugs in the code that spends excessive energy and also to
optimize the code for energy consumption.

--------- NOTE -----------------------------------------
The following devices do NOT support code correlation as they do not support SWO. 
So you don't need to read on (unless you are curious:)) if you run profiler on one
of these:
- All EFM8 devices.
- ARM devices that are based on Cortex-M0/Cortex-M0+, e.g. Zero Gecko kit.
--------------------------------------------------------


This functionality is possible due to the AEM hardware and program counter (PC) 
samples being output by the processor. For ARM devices PC samples can be output
over the SWO interface. To enable PC sample output over SWO, the ITM and TPIU 
peripherals needs to be correctly configured. The code to do this configuration
is located in the EFM32 SDK within the BSP_TraceSwoSetup() (see bsp_trace.h). 

The AEM graphing is independent of the PC samples, and can be used at any time.

If you wish to enable code correlation for your own application code, you need to follow
the instruction below:
 
1.Your code must enable SWO output from the EFM32 MCU. To enable this output,
  simply call the function BSP_TraceSwoSetup() early in your main() function.
  
2.Your program must be built with debug information enabled so that source code
  lookup is possible.
  - If you create your project in Simplicity Studio, this is enabled by default.
  - If you import a project into Studio, check compiler options in project context
    menu "Properties...-> C/C++ Build -> Settings".
  - If you build your program out of Simplicity Studio, check compiler options of
    the build tools.
